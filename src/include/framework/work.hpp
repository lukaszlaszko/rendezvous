#pragma once

#include <framework/dispatcher.hpp>
#include <framework/timers.hpp>

#include <chrono>
#include <list>


namespace framework {

/**
 * @brief Represents a piece of work which can be scheduled for dispatched execution.
 * @details
 * Work allows subsequent registrations of the same callback with dispatch queue or one_one_timer. It's intended to
 * act as a helper for this sort of routines.
 */
class work
{
public:
    /**
     * @brief Callback type.
     */
    using callback_type = std::function<void()>;

    /**
     * @brief Creates an instance of this work.
     * @param dispatcher Dispatcher woirk will be dispatched with.
     * @param callback Callback representing actual work.
     */
    explicit work(dispatcher& dispatcher, callback_type&& callback);

    /**
     * @brief Schedules callback for one time execution through underlying dispatcher.
     * @details
     * This is an equivalent to **dispatcher::call_soon**.
     */
    void do_soon();
    /**
     * @brief Schedules underlying callback for delayed execution.
     * @details
     * This is an equivalent to creation of **framework::one_shot_timer** and call to **schedule** method.
     *
     * Repeated call to this method before actual delay will reschedule invocation with the most recent delay.
     *
     * @tparam rep Rep, an arithmetic type representing the number of ticks
     * @tparam period Tick period.
     * @param delay Minimum delay after which callback will be invoked.
     */
    template <typename rep, typename period>
    void do_in(const std::chrono::duration<rep, period>& delay);

private:
    callback_type callback_;
    dispatcher& dispatcher_;

    one_shot_timer timer_;

};

}

#include "work.ipp"
