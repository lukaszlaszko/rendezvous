#pragma once

#include "work.hpp"


namespace framework {

inline work::work(dispatcher& dispatcher, callback_type&& callback)
    :
        dispatcher_(dispatcher),
        callback_(std::move(callback)),
        timer_(dispatcher, [this](auto&) { callback_(); })
{

}

inline void work::do_soon()
{
    dispatcher_.call_soon([this]() { callback_(); });
}

template <typename rep, typename period>
inline void work::do_in(const std::chrono::duration<rep, period>& delay)
{
    timer_.schedule(delay);
}

}
