#pragma once

#include <cerrno>
#include <chrono>
#include <cstring>
#include <sys/epoll.h>
#include <unistd.h>
#include <utility>


namespace framework {

inline dispatcher::dispatcher(error_callback_type error_callback)
    :
        dispatchable(error_callback)
{
    dispatch_tokens_ = std::make_shared<std::queue<dispatch_token>>();

    auto epoll_fd = ::epoll_create1(0);
    check_lethal_error(epoll_fd);

    dispatchable::set_fd(epoll_fd);
}

inline dispatcher::dispatcher(
        dispatcher& parent,
        error_callback_type error_callback)
    :
        dispatcher(error_callback)
{
    dispatch_tokens_ = parent.dispatch_tokens_;

    parent_ = &parent;
    parent.add(*this, true, false, false, false);
}

inline dispatcher::~dispatcher()
{
    if (parent_ != nullptr)
        parent_->remove(*this);
}

inline void dispatcher::add(
        const dispatchable& tr,
        bool can_receive,
        bool can_send,
        bool can_disconnect,
        bool edge_triggered)
{
    epoll_event event{};
    if (can_receive)
        event.events |= EPOLLIN;
    if (can_send)
        event.events |= EPOLLOUT;
    if (can_disconnect)
        event.events |= EPOLLHUP;
    if (edge_triggered)
        event.events |= EPOLLET;
    event.data.fd = tr.fd();
    event.data.ptr = const_cast<dispatchable*>(&tr);

    auto operation = EPOLL_CTL_MOD;
    if (dispatchables_.find(tr.fd()) == dispatchables_.end())
    {
        dispatchables_.emplace(tr.fd());
        operation = EPOLL_CTL_ADD;
    }

    auto ctl_result = ::epoll_ctl(fd(), operation, tr.fd(), &event);
    check_error(ctl_result);
}

inline void dispatcher::remove(const dispatchable& tr)
{
    auto search_result = dispatchables_.find(tr.fd());
    if (search_result != dispatchables_.end())
    {
        dispatchables_.erase(search_result);

        epoll_event event{};
        auto ctl_result = ::epoll_ctl(fd(), EPOLL_CTL_DEL, tr.fd(), &event);
        check_error(ctl_result);
    }
}

template <typename callable_type>
inline void dispatcher::call_soon(callable_type&& callable)
{
    dispatch_tokens_->emplace(std::move(callable));
}

template <int max_events>
inline bool dispatcher::dispatch(milliseconds timeout)
{
    auto dispatched = 0;
    if (dispatch_tokens_->empty())
    {
        auto dispatched_events = dispatch_epoll(max_events - dispatched, timeout);
        dispatched += dispatched_events;
    }
    else
    {
        dispatch_tokens_->emplace(); // add epoll request to the queue
        while (dispatched < max_events && !dispatch_tokens_->empty())
        {
            auto& token = dispatch_tokens_->front();
            if (token.is_callable())
            {
                dispatch_callback(token.callback());
                dispatched++;
            }
            else
            {
                auto dispatched_events = dispatch_epoll(max_events - dispatched, timeout);
                dispatched += dispatched_events;
            }

            dispatch_tokens_->pop();
        }
    }

    return dispatched > 0;
}

template <int max_events>
inline bool dispatcher::dispatch()
{
    return dispatch<max_events>(milliseconds::zero());
}

inline dispatcher::dispatch_token::dispatch_token(std::function<void()>&& callback)
    :
        is_callable_(true),
        callback_(callback)
{

}

inline bool dispatcher::dispatch_token::is_callable() const
{
    return is_callable_;
}

inline const std::function<void()>& dispatcher::dispatch_token::callback() const
{
    return callback_;
}

inline void dispatcher::handle_event(
        bool should_receive,
        bool should_send,
        bool should_disconnect)
{
    dispatch(milliseconds::zero());
}

inline int dispatcher::dispatch_epoll(int max_events, milliseconds timeout)
{
    epoll_event events[max_events];
    auto wait_result = ::epoll_wait(fd(), events, max_events, timeout.count());
    check_error(wait_result);

    for (auto i = 0; i < wait_result; i++)
    {
        auto& event = events[i];
        auto target = reinterpret_cast<dispatchable*>(event.data.ptr);

        target->handle_event(
                (event.events & EPOLLIN) == EPOLLIN,
                (event.events & EPOLLOUT) == EPOLLOUT,
                (event.events & EPOLLHUP) == EPOLLHUP);
    }

    return wait_result;
}

inline void dispatcher::dispatch_callback(const std::function<void()>& callback)
{
    try
    {
        if (callback)
            callback();
    }
    catch (...)
    {
        raise_error(0);
    }
}

}

