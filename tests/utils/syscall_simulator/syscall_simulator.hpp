#pragma once

#include <functional>

#include <syscall.h>


class syscall_interceptor
{
public:
    template <typename T>
    struct identity
    {
        using type = T;
    };

    using callback_type = std::function<bool(
            long& arg0,
            long& arg1,
            long& arg2,
            long& arg3,
            long& arg4,
            long& arg5,
            long& result)>;

    syscall_interceptor(long syscall_number, callback_type&& callback);

    template <typename... args_types>
    syscall_interceptor(
            long syscall_number,
            typename identity<std::function<bool(long& result, args_types...)>>::type&& callback);

    ~syscall_interceptor();

private:
    template <typename function_type, typename tuple_type, std::size_t... Is>
    static bool invoke(function_type&& function, long& result, tuple_type args, std::index_sequence<Is...>);

    long syscall_number_{0l};
    callback_type callback_;

};

#include "syscall_simulator.ipp"
