#include <framework/loop.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <csignal>
#include <thread>

using namespace std;
using namespace testing;


TEST(loop, create)
{
    framework::dispatcher dispatcher;
    framework::loop loop(dispatcher);
}

//TEST(loop, interrupt_on)
//{
//    framework::dispatcher dispatcher;
//    framework::loop loop(dispatcher);
//    loop.interrupt_on(SIGUSR1);
//    
//    auto finished = false;
//    thread th(
//        [&loop, &finished]()
//        {
//            loop.run_forever();
//            finished = true;
//        });
//    ASSERT_FALSE(finished);
//    
//    raise(SIGUSR1);
//    this_thread::sleep_for(500ms);
//    
//    ASSERT_TRUE(finished);
//}

