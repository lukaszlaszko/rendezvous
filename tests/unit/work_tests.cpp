#include <framework/work.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <chrono>
#include <thread>

using namespace std;


TEST(work, create)
{
    framework::dispatcher dispatcher;
    framework::work work(dispatcher, {});
}

TEST(work, do_soon__multiple_times)
{
    framework::dispatcher dispatcher;

    auto called = 0ul;
    framework::work work(dispatcher, [&](){ called++; });
    ASSERT_EQ(called, 0ul);

    work.do_soon();
    ASSERT_EQ(called, 0ul);

    dispatcher.dispatch();
    ASSERT_EQ(called, 1ul);

    dispatcher.dispatch();
    ASSERT_EQ(called, 1ul);

    work.do_soon();
    dispatcher.dispatch();
    ASSERT_EQ(called, 2ul);
}

TEST(work, do_in)
{
    framework::dispatcher dispatcher;

    auto called = 0ul;
    framework::work work(dispatcher, [&](){ called++; });
    ASSERT_EQ(called, 0ul);

    work.do_in(200ms);
    dispatcher.dispatch();
    ASSERT_EQ(called, 0ul);

    this_thread::sleep_for(500ms);
    dispatcher.dispatch();
    ASSERT_EQ(called, 1ul);
}

