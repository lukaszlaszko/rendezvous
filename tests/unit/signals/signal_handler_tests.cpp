#include <framework/dispatcher.hpp>
#include <framework/signals/signal_handler.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <csignal>
#include <vector>

using namespace std;
using namespace testing;


TEST(signal_handler, create_single_for_SIGUSR1)
{
    vector<int> handled_signals;

    framework::dispatcher dispatcher;
    framework::signal_handler handler(
            dispatcher,
            SIGUSR1,
            [&handled_signals](auto signo)
            {
                handled_signals.push_back(signo);
            });

    ASSERT_TRUE(handled_signals.empty());

    raise(SIGUSR1);
    ASSERT_TRUE(handled_signals.empty());

    ASSERT_TRUE(dispatcher.dispatch(0ms));
    ASSERT_FALSE(handled_signals.empty());
    ASSERT_EQ(handled_signals.size(), 1ul);
    ASSERT_EQ(handled_signals.at(0ul), SIGUSR1);
}

TEST(signal_handler, create_double_for_SIGUSR1_SIGUSR2)
{
    vector<int> handled_signals;

    framework::dispatcher dispatcher;
    framework::signal_handler handler_1(
            dispatcher,
            SIGUSR1,
            [&handled_signals](auto signo)
            {
                handled_signals.push_back(signo);
            });
    framework::signal_handler handler_2(
            dispatcher,
            SIGUSR2,
            [&handled_signals](auto signo)
            {
                handled_signals.push_back(signo);
            });

    ASSERT_TRUE(handled_signals.empty());

    raise(SIGUSR1);
    ASSERT_TRUE(handled_signals.empty());

    ASSERT_TRUE(dispatcher.dispatch(0ms));
    ASSERT_FALSE(handled_signals.empty());
    ASSERT_EQ(handled_signals.size(), 1ul);
    ASSERT_EQ(handled_signals.at(0ul), SIGUSR1);

    raise(SIGUSR2);
    ASSERT_TRUE(dispatcher.dispatch(0ms));
    ASSERT_FALSE(handled_signals.empty());
    ASSERT_EQ(handled_signals.size(), 2ul);
    ASSERT_THAT(handled_signals, ElementsAre(SIGUSR1, SIGUSR2));
}

