#include <framework/dispatcher.hpp>
#include <framework/timers/one_shot_timer.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <thread>

using namespace std;
using namespace testing;


TEST(one_shot_timer, schedule__with_milliseconds)
{
    auto fire_count = 0;
    
    framework::dispatcher dispatcher;
    framework::one_shot_timer timer(
            dispatcher,
            [&fire_count](auto& source) { fire_count++; });
            
    timer.schedule(100ms);
    this_thread::sleep_for(500ms);
    
    ASSERT_EQ(fire_count, 0);
    
    dispatcher.dispatch(0ms);
    ASSERT_EQ(fire_count, 1);
    
    dispatcher.dispatch(0ms);
    ASSERT_EQ(fire_count, 1);
}

TEST(one_shot_timer, schedule__already_scheduled)
{
    auto fire_count = 0;
    
    framework::dispatcher dispatcher;
    framework::one_shot_timer timer(
            dispatcher,
            [&fire_count](auto& source) { fire_count++; });
            
    timer.schedule(2min);
    this_thread::sleep_for(500ms);
    
    ASSERT_EQ(fire_count, 0);
    
    timer.schedule(100ms);
    this_thread::sleep_for(500ms);
    
    dispatcher.dispatch(0ms);
    ASSERT_EQ(fire_count, 1);
    
    dispatcher.dispatch(0ms);
    ASSERT_EQ(fire_count, 1);
}

TEST(one_shot_timer, is_armed)
{
    auto fire_count = 0;
    
    framework::dispatcher dispatcher;
    framework::one_shot_timer timer(
            dispatcher,
            [&fire_count](auto& source) { fire_count++; });
     
    ASSERT_FALSE(timer.is_armed());
            
    timer.schedule(2min);
    ASSERT_TRUE(timer.is_armed());
    
    timer.disarm();
    ASSERT_FALSE(timer.is_armed());
}

