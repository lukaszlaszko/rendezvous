#include <framework/dispatcher.hpp>
#include <framework/timers/auto_reload_timer.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <thread>

using namespace std;
using namespace testing;

TEST(auto_reload_timer, create__with_milliseconds)
{
    framework::dispatcher dispatcher;
    framework::auto_reload_timer timer(
            dispatcher,
            100ms,
            [](auto& source, auto count){ });
            
    ASSERT_GT(timer.fd(), 0);
    ASSERT_FALSE(timer.is_armed());
}

TEST(auto_reload_timer, arm)
{
    auto fire_count = 0;
    
    framework::dispatcher dispatcher;
    framework::auto_reload_timer timer(
            dispatcher,
            100ms,
            [&fire_count](auto& source, auto count)
            {
                fire_count += count;
            });
            
    timer.arm();
            
    this_thread::sleep_for(500ms);
    ASSERT_EQ(fire_count, 0);
    
    dispatcher.dispatch(0ms);
    ASSERT_GE(fire_count, 4);
}

TEST(auto_reload_timer, create__is_armed)
{
    framework::dispatcher dispatcher;
    framework::auto_reload_timer timer(
            dispatcher,
            1h,
            [](auto& source, auto count){ });
            
    ASSERT_FALSE(timer.is_armed());
    
    timer.arm();
    ASSERT_TRUE(timer.is_armed());
    
    timer.disarm();
    ASSERT_FALSE(timer.is_armed());
}