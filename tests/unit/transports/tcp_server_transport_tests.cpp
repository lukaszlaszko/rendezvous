#include "utils/tcp_socket_guard.hpp"

#include <framework/dispatcher.hpp>
#include <framework/transports/network/ipv4.hpp>
#include <framework/transports/tcp_server_transport.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <boost/concurrency.hpp>
#include <boost/memory.hpp>
#include <boost/optional.hpp>

#include <array>
#include <chrono>
#include <cerrno>
#include <list>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sstream>
#include <string>
#include <thread>

using namespace std;
using namespace std::chrono;
using namespace testing;
using namespace boost::concurrency;
using namespace boost::memory;

using ipv4_traits = framework::ip::v4::traits;
using spsc_ring_buffer = spsc_variable_circular_buffer<>;
using ipv4_tcp_server_transport = framework::tcp_server_transport<ipv4_traits, spsc_ring_buffer>;


TEST(tcp_server_transport, create_on_new_port)
{
    framework::dispatcher dispatcher;
    ipv4_tcp_server_transport transport(
            dispatcher,
            {}, // options
            "0.0.0.0:0", // create on random port
            {}, // connected callback,
            {}, // disconnected callback,
            {}, // received callback,
            [](auto& error)
            {
                FAIL();
            }); // error callback
}

TEST(tcp_server_transport, create_on_existing_port)
{
    framework::dispatcher dispatcher;
    ipv4_tcp_server_transport transport_1(
            dispatcher,
            {}, // options
            "0.0.0.0:0",
            {}, // connected callback,
            {}, // disconnected callback,
            {}, // received callback,
            [](auto& error)
            {
                FAIL();
            }); // error callback

    auto error_count = 0;
    ASSERT_THROW(
    {
        ipv4_tcp_server_transport transport_2(
                dispatcher,
                {}, // options
                transport_1.at(),
                {}, // connected callback,
                {}, // disconnected callback,
                {}, // received callback,
                [&error_count](auto& error)
                {
                    error_count++;
                }); // error callback
    }, framework::rendezvous_error);

    ASSERT_EQ(error_count, 1);
}

TEST(tcp_server_transport, create_on_selected_interface)
{
    framework::dispatcher dispatcher;
    ipv4_tcp_server_transport transport_1(
            dispatcher,
            {}, // options
            "127.0.0.1:0",
            {}, // connected callback,
            {}, // disconnected callback,
            {}, // received callback,
            [](auto& error)
            {
                FAIL();
            }); // error callback
}

TEST(tcp_server_transport, connect_single_client)
{
    auto connected_count = 0;

    framework::dispatcher dispatcher;
    ipv4_tcp_server_transport transport(
            dispatcher,
            {}, // options
            "0.0.0.0:0",
            [&connected_count](auto& client_transport)
            {
                connected_count++;
            }, // connected callback,
            {}, // disconnected callback,
            {}, // received callback,
            [](auto& error)
            {
                FAIL();
            }); // error callback

    auto client_socket = tcp_socket_guard::connect_to(transport.at());

    ASSERT_EQ(connected_count, 0);

    dispatcher.dispatch();
    ASSERT_EQ(connected_count, 1);
}

/**
 * Tests connection of multiple clients in single dispatch
 */
TEST(tcp_server_transport, connect_multiple_clients__one_dispatch)
{
    auto connected_count = 0;

    framework::dispatcher dispatcher;
    ipv4_tcp_server_transport transport(
            dispatcher,
            {}, // options
            "0.0.0.0:0",
            [&connected_count](auto& client_transport)
            {
                connected_count++;
            }, // connected callback,
            {}, // disconnected callback,
            {}, // received callback,
            [](auto& error)
            {
                FAIL();
            }); // error callback

    auto client_socket_1 = tcp_socket_guard::connect_to(transport.at());
    auto client_socket_2 = tcp_socket_guard::connect_to(transport.at());

    ASSERT_EQ(connected_count, 0);

    dispatcher.dispatch();
    ASSERT_EQ(connected_count, 2);
}

/**
 * Tests connection of multiple clients in multiple dispatches
 */
TEST(tcp_server_transport, connect_multiple_clients__multiple_dispatches)
{
    auto connected_count = 0;

    framework::dispatcher dispatcher;
    ipv4_tcp_server_transport transport(
            dispatcher,
            {}, // options
            "0.0.0.0:0",
            [&connected_count](auto& client_transport)
            {
                connected_count++;
            }, // connected callback,
            {}, // disconnected callback,
            {}, // received callback,
            [](auto& error)
            {
                FAIL();
            }); // error callback

    auto client_socket_1 = tcp_socket_guard::connect_to(transport.at());

    ASSERT_EQ(connected_count, 0);

    dispatcher.dispatch();
    ASSERT_EQ(connected_count, 1);

    auto client_socket_2 = tcp_socket_guard::connect_to(transport.at());

    dispatcher.dispatch();
    ASSERT_EQ(connected_count, 2);
}

TEST(tcp_server_transport, receive)
{
    const auto port = 45004;

    auto connected_count = 0;
    auto received_count = 0;

    string data("abcd123");

    framework::dispatcher dispatcher;
    ipv4_tcp_server_transport transport(
            dispatcher,
            {}, // options
            "0.0.0.0:" + to_string(port),
            [&connected_count](auto& client_transport)
            {
                connected_count++;
            }, // connected callback,
            {}, // disconnected callback,
            [&received_count, &data](auto& source, buffer_ref& buffer)
            {
                received_count++;

                EXPECT_EQ(buffer.length(), data.length());
                EXPECT_EQ(memcmp(buffer.as_pointer<char*>(), data.data(), data.length()), 0);
                EXPECT_NE(source.info().from(), ipv4_traits::endpoint_type("0.0.0.0:0"));

                EXPECT_EQ(source.info().receive_time(), system_clock::time_point(0ns));
                EXPECT_EQ(source.info().receive_time(), system_clock::time_point(0ns));
            }, // received callback,
            [](auto& error)
            {
                FAIL();
            }); // error callback

    auto client_socket = tcp_socket_guard::connect_to(transport.at());

    ASSERT_EQ(connected_count, 0);

    dispatcher.dispatch();
    ASSERT_EQ(connected_count, 1);

    ASSERT_GE(client_socket.send(data), 0);

    ASSERT_EQ(received_count, 0);
    dispatcher.dispatch();

    ASSERT_EQ(received_count, 1);
}

TEST(tcp_server_transport, send_callback_ring_buffer_full)
{
    const auto port = 45007;
}

TEST(tcp_server_transport, disconnect)
{
    auto connected_count = 0;
    auto disconnected_count = 0;
    boost::optional<ipv4_tcp_server_transport::connected_client_transport&> connected_client;

    framework::dispatcher dispatcher;
    ipv4_tcp_server_transport transport(
            dispatcher,
            {}, // options
            "0.0.0.0:0",
            [&connected_count, &connected_client](auto& client_transport)
            {
                connected_count++;
                connected_client = client_transport;
            }, // connected callback,
            [&disconnected_count, &connected_client](auto& source)
            {
                disconnected_count++;
                connected_client = boost::none;
            }, // disconnected callback
            [&disconnected_count](auto& source, auto& buffer)
            {
                FAIL();
            }, // received callback,
            [](auto& error)
            {
                FAIL();
            }); // error callback

    auto client_socket = tcp_socket_guard::connect_to(transport.at());

    ASSERT_EQ(connected_count, 0);

    dispatcher.dispatch();
    ASSERT_EQ(connected_count, 1);
    ASSERT_NE(connected_client, boost::none);

    connected_client->disconnect();

    ASSERT_EQ(disconnected_count, 0);
    auto client_socket_fd = connected_client->fd();

    dispatcher.dispatch();
    ASSERT_EQ(disconnected_count, 1);
    ASSERT_EQ(connected_client, boost::none);
}

TEST(tcp_server_transport, disconnected_by_client)
{
    auto connected_count = 0;
    auto disconnected_count = 0;
    boost::optional<ipv4_tcp_server_transport::connected_client_transport&> connected_client;

    framework::dispatcher dispatcher;
    ipv4_tcp_server_transport transport(
            dispatcher,
            {}, // options
            "0.0.0.0:0",
            [&connected_count, &connected_client](auto& client_transport)
            {
                connected_count++;
                connected_client = client_transport;
            }, // connected callback,
            [&disconnected_count, &connected_client](auto& source)
            {
                disconnected_count++;
                connected_client = boost::none;
            }, // disconnected callback
            [&disconnected_count](auto& source, auto& buffer)
            {
                FAIL();
            }, // received callback,
            [](auto& error)
            {
                FAIL();
            }); // error callback

    auto client_socket = tcp_socket_guard::connect_to(transport.at());

    ASSERT_EQ(connected_count, 0);

    dispatcher.dispatch();
    ASSERT_EQ(connected_count, 1);
    ASSERT_NE(connected_client, boost::none);

    client_socket.force_close();

    ASSERT_EQ(disconnected_count, 0);
    ASSERT_NE(connected_client, boost::none);

    dispatcher.dispatch();

    ASSERT_EQ(disconnected_count, 1);
    ASSERT_EQ(connected_client, boost::none);
}

TEST(tcp_server_transport, close)
{
    static const auto fcntl_error = -1;

    framework::dispatcher dispatcher;
    ipv4_tcp_server_transport transport(
            dispatcher,
            {}, // options
            "0.0.0.0:0",
            {},
            {},
            {});

    transport.close();

    auto result = fcntl(transport.fd(), F_GETFL);
    ASSERT_EQ(result, fcntl_error);
}

TEST(tcp_server_transport, wait)
{
    auto connected_count = 0;
    auto disconnected_count = 0;
    boost::optional<ipv4_tcp_server_transport::connected_client_transport&> connected_client;

    framework::dispatcher dispatcher;
    framework::tcp_options options;
    options.sp_sndbuf = 1024;

    ipv4_tcp_server_transport transport(
            dispatcher,
            options,
            "0.0.0.0:0",
            [&connected_count, &connected_client](auto& client_transport)
            {
                connected_count++;
                connected_client = client_transport;
            }, // connected callback,
            [&disconnected_count, &connected_client](auto& source)
            {
                disconnected_count++;
                connected_client = boost::none;
            }, // disconnected callback
            [&disconnected_count](auto& source, auto& buffer)
            {
                FAIL();
            }, // received callback,
            [](auto& error)
            {
                FAIL();
            }); // error callback

    array<char, 1000> data;
    generate_n(data.begin(), data.size(), rand);

    auto client_socket = tcp_socket_guard::connect_to(transport.at());

    ASSERT_EQ(connected_count, 0);

    dispatcher.dispatch();
    ASSERT_EQ(connected_count, 1);
    ASSERT_NE(connected_client, boost::none);

    for (auto i = 0; i < 1000; i++)
        connected_client->send(buffer_ref(data.data(), data.size()));
    ASSERT_FALSE(connected_client->wait(1ms));
    ASSERT_TRUE(connected_client->has_pending());

    connected_client->wait(1s);

    dispatcher.dispatch();
    ASSERT_TRUE(connected_client->wait(1ms));
}

TEST(tcp_server_transport, connected_client_transport__operators)
{
    auto connected_count = 0;
    vector<ipv4_tcp_server_transport::connected_client_transport*> connected;

    framework::dispatcher dispatcher;
    ipv4_tcp_server_transport transport(
            dispatcher,
            {}, // options
            "0.0.0.0:0",
            [&connected](auto& client_transport)
            {
                connected.emplace_back(&client_transport);
            }, // connected callback,
            {}, // disconnected callback,
            {}, // received callback,
            [](auto& error)
            {
                FAIL();
            }); // error callback

    auto client_socket_1 = tcp_socket_guard::connect_to(transport.at());
    auto client_socket_2 = tcp_socket_guard::connect_to(transport.at());

    dispatcher.dispatch();
    dispatcher.dispatch();
    ASSERT_THAT(connected, SizeIs(2u));

    ASSERT_TRUE(*connected[0] == *connected[0]);
    ASSERT_FALSE(*connected[0] == *connected[1]);
    ASSERT_FALSE(*connected[1] == *connected[0]);

    ASSERT_TRUE(*connected[0] < *connected[1]);
    ASSERT_FALSE(*connected[0] < *connected[0]);
    ASSERT_FALSE(*connected[1] < *connected[0]);
}

TEST(tcp_server_transport, connected_client_transport__closure__pointer)
{
    auto connected_count = 0;
    vector<ipv4_tcp_server_transport::connected_client_transport*> connected;

    string closure("some closure");

    framework::dispatcher dispatcher;
    ipv4_tcp_server_transport transport(
            dispatcher,
            {}, // options
            "0.0.0.0:0",
            [&connected, &closure](auto& client_transport)
            {
                client_transport.set_closure(&closure);
                connected.push_back(&client_transport);
            }, // connected callback,
            {}, // disconnected callback,
            {}, // received callback,
            [](auto& error)
            {
                FAIL();
            }); // error callback

    auto client_socket = tcp_socket_guard::connect_to(transport.at());

    dispatcher.dispatch();
    ASSERT_THAT(connected, SizeIs(1u));

    ASSERT_TRUE(connected[0]->closure<string>());
    ASSERT_EQ(&*connected[0]->closure<string>(), &closure);
}

TEST(tcp_server_transport, connected_client_transport__closure__rvalue)
{
    auto connected_count = 0;
    vector<ipv4_tcp_server_transport::connected_client_transport*> connected;

    framework::dispatcher dispatcher;
    ipv4_tcp_server_transport transport(
            dispatcher,
            {}, // options
            "0.0.0.0:0",
            [&connected](auto& client_transport)
            {
                client_transport.set_closure(string("some closure"));
                connected.push_back(&client_transport);
            }, // connected callback,
            {}, // disconnected callback,
            {}, // received callback,
            [](auto& error)
            {
                FAIL();
            }); // error callback

    auto client_socket = tcp_socket_guard::connect_to(transport.at());

    dispatcher.dispatch();
    ASSERT_THAT(connected, SizeIs(1u));

    ASSERT_TRUE(connected[0]->closure<string>());
    ASSERT_EQ(*connected[0]->closure<string>(), "some closure");
}

TEST(tcp_server_transport, connected_client_transport__id)
{
    auto connected_count = 0;
    vector<ipv4_tcp_server_transport::connected_client_transport*> connected;

    framework::dispatcher dispatcher;
    ipv4_tcp_server_transport transport(
            dispatcher,
            {}, // options
            "0.0.0.0:0",
            [&connected](auto& client_transport)
            {
                client_transport.set_closure(string("some closure"));
                connected.push_back(&client_transport);
            }, // connected callback,
            {}, // disconnected callback,
            {}, // received callback,
            [](auto& error)
            {
                FAIL();
            }); // error callback

    auto client_socket_1 = tcp_socket_guard::connect_to(transport.at());
    auto client_socket_2 = tcp_socket_guard::connect_to(transport.at());

    dispatcher.dispatch();
    dispatcher.dispatch();
    ASSERT_THAT(connected, SizeIs(2u));

    ASSERT_GT(connected.at(0)->id(), 0);
    ASSERT_GT(connected.at(1)->id(), 0);
    ASSERT_NE(connected.at(0)->id(), connected.at(1)->id());
}

TEST(tcp_server_transport, connected_client_transport__from)
{
    auto connected_count = 0;
    vector<ipv4_tcp_server_transport::connected_client_transport*> connected;

    framework::dispatcher dispatcher;
    ipv4_tcp_server_transport transport(
            dispatcher,
            {}, // options
            "0.0.0.0:0",
            [&connected](auto& client_transport)
            {
                client_transport.set_closure(string("some closure"));
                connected.push_back(&client_transport);
            }, // connected callback,
            {}, // disconnected callback,
            {}, // received callback,
            [](auto& error)
            {
                FAIL();
            }); // error callback

    auto client_socket_1 = tcp_socket_guard::connect_to(transport.at());
    auto client_socket_2 = tcp_socket_guard::connect_to(transport.at());

    dispatcher.dispatch();
    dispatcher.dispatch();
    ASSERT_THAT(connected, SizeIs(2u));

    ASSERT_GT(connected.at(0)->from().get_port(), 0u);
    ASSERT_GT(connected.at(1)->from().get_port(), 0u);
    ASSERT_EQ(connected.at(0)->from().get_address(), connected.at(1)->from().get_address());
    ASSERT_NE(connected.at(0)->from().get_port(), connected.at(1)->from().get_port());
}