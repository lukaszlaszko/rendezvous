#include "utils/transport_helpers.hpp"
#include "utils/udp_socket_guard.hpp"

#include <framework/dispatcher.hpp>
#include <framework/transports/network/ipv4.hpp>
#include <framework/transports/udp_server_transport.hpp>
#include <syscall_simulator/syscall_simulator.hpp>

#include <boost/concurrency.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <string>

using namespace std;
using namespace boost::memory;

using ipv4_traits = framework::ip::v4::traits;
using spsc_ring_buffer = boost::concurrency::spsc_variable_circular_buffer<>;
using ipv4_udp_server_transport = framework::udp_server_transport<ipv4_traits, spsc_ring_buffer>;


TEST(udp_server_transport, create)
{
    framework::dispatcher dispatcher;
    ipv4_udp_server_transport transport(
            dispatcher,
            {}, // options
            "0.0.0.0:0", // create on random port
            { });

    ASSERT_EQ(transport.at().get_address(), "0.0.0.0");
    ASSERT_NE(transport.at().get_port(), 0u);
}

TEST(udp_server_transport, create_on_selected_interface)
{
    framework::dispatcher dispatcher;
    ipv4_udp_server_transport transport(
            dispatcher,
            {}, // options
            "127.0.0.1:0", // create on random port
            { });

    ASSERT_EQ(transport.at().get_address(), "127.0.0.1");
    ASSERT_NE(transport.at().get_port(), 0u);
}

TEST(udp_server_transport, create_on_existing_port)
{
    framework::dispatcher dispatcher;
    ipv4_udp_server_transport transport_1(
            dispatcher,
            {}, // options
            "0.0.0.0:0", // create on random port
            { },  // received callback,
            [](auto& error)
            {
                FAIL();
            });

    auto error_count = 0;
    ASSERT_THROW(
    {
        ipv4_udp_server_transport transport_2(
                dispatcher,
                {}, // options
                transport_1.at(),
                {}, // received callback,
                [&error_count](auto& error)
                {
                    error_count++;
                }); // error callback
    }, framework::rendezvous_error);

    ASSERT_EQ(error_count, 1);
}

TEST(udp_server_transport, receive_single_source)
{
    auto test_data = "abcdfer";
    auto receive_count = 0;
    string received_data;

    framework::dispatcher dispatcher;
    ipv4_udp_server_transport transport(
            dispatcher,
            {}, // options
            "0.0.0.0:0", // create on random port
            [&receive_count, &received_data](auto& source, buffer_ref& data)
            {
                receive_count++;
                received_data.clear();
                received_data.append(data.as_pointer<char*>(), data.length());
            },
            [](auto& error)
            {
                FAIL();
            });

    udp_socket_guard client_socket;
    client_socket.bind();
    client_socket.sendto(transport.at(), test_data);

    ASSERT_EQ(receive_count, 0);

    dispatcher.dispatch();
    ASSERT_EQ(receive_count, 1);
    ASSERT_EQ(received_data, test_data);
    ASSERT_EQ(transport.info().from().get_port(), client_socket.port());
}

TEST(udp_server_transport, receive_multiple_sources)
{
    auto test_data_1 = "abcd";
    auto test_data_2 = "12345";
    auto receive_count = 0;
    string received_data;

    framework::dispatcher dispatcher;
    ipv4_udp_server_transport transport(
            dispatcher,
            {}, // options
            "0.0.0.0:0", // create on random port
            [&receive_count, &received_data](auto& source, buffer_ref& data)
            {
                receive_count++;
                received_data.clear();
                received_data.append(data.as_pointer<char*>(), data.length());
            },
            [](auto& error)
            {
                FAIL();
            });

    udp_socket_guard client_socket_1;
    udp_socket_guard client_socket_2;
    client_socket_1.sendto(transport.at(), test_data_1);

    ASSERT_EQ(receive_count, 0);

    dispatcher.dispatch();
    ASSERT_EQ(receive_count, 1);
    ASSERT_EQ(received_data, test_data_1);

    client_socket_2.sendto(transport.at(), test_data_2);

    ASSERT_EQ(receive_count, 1);

    dispatcher.dispatch();
    ASSERT_EQ(receive_count, 2);
    ASSERT_EQ(received_data, test_data_2);
}

TEST(udp_server_transport, reply_with_referenceable)
{
    auto test_data = "abcdfer";
    auto reply_data = "123456";
    auto receive_count = 0;

    framework::dispatcher dispatcher;
    ipv4_udp_server_transport transport(
            dispatcher,
            {}, // options
            "0.0.0.0:0", // create on random port
            [&receive_count, &reply_data](auto& source, auto& data)
            {
                receive_count++;
                string s(reply_data);
                source.reply(s);
            },
            [](auto& error)
            {
                FAIL();
            });

    udp_socket_guard client_socket;
    client_socket.bind();
    client_socket.sendto(transport.at(), test_data);

    ASSERT_EQ(receive_count, 0);

    dispatcher.dispatch();
    ASSERT_EQ(receive_count, 1);
    ASSERT_EQ(transport.info().from().get_port(), client_socket.port());

    auto received_reply = client_socket.recv();
    ASSERT_NE(received_reply, boost::none);
    ASSERT_EQ(*received_reply, reply_data);
}

TEST(udp_server_transport, reply_with_movable)
{
    auto test_data = "abcdfer";
    auto reply_data = "123456";
    auto receive_count = 0;

    framework::dispatcher dispatcher;
    ipv4_udp_server_transport transport(
            dispatcher,
            {}, // options
            "0.0.0.0:0", // create on random port
            [&receive_count, &reply_data](auto& source, auto& data)
            {
                receive_count++;
                source.reply(string(reply_data));
            },
            [](auto& error)
            {
                FAIL();
            });

    udp_socket_guard client_socket;
    client_socket.bind();
    client_socket.sendto(transport.at(), test_data);

    ASSERT_EQ(receive_count, 0);

    dispatcher.dispatch();
    ASSERT_EQ(receive_count, 1);
    ASSERT_EQ(transport.info().from().get_port(), client_socket.port());
    ASSERT_EQ(transport.info().from().get_address(), "127.0.0.1");

    auto received_reply = client_socket.recv();
    ASSERT_NE(received_reply, boost::none);
    ASSERT_EQ(*received_reply, reply_data);
}

TEST(udp_server_transport, reply_EAGAIN)
{
    auto test_data = "abcdfer";
    auto reply_data = "123456";
    auto receive_count = 0;

    framework::dispatcher dispatcher;
    ipv4_udp_server_transport transport(
            dispatcher,
            {}, // options
            "0.0.0.0:0", // create on random port
            [&receive_count, &reply_data](auto& source, auto& data)
            {
                syscall_interceptor sendto_guard(
                        SYS_sendto,
                        [](long& result)
                        {
                            result = -EAGAIN;
                            return true;
                        });

                receive_count++;
                source.reply(string(reply_data));
            },
            [](auto& error)
            {
                FAIL();
            });

    udp_socket_guard client_socket;
    client_socket.bind();
    client_socket.sendto(transport.at(), test_data);

    ASSERT_EQ(receive_count, 0);

    dispatcher.dispatch();
    ASSERT_TRUE(transport.has_pending());
    ASSERT_EQ(receive_count, 1);
    ASSERT_EQ(transport.info().from().get_port(), client_socket.port());
    ASSERT_EQ(transport.info().from().get_address(), "127.0.0.1");
}

TEST(udp_server_transport, close)
{
    static const auto fcntl_error = -1;

    framework::dispatcher dispatcher;
    ipv4_udp_server_transport transport(
            dispatcher,
            {}, // options
            "0.0.0.0:0", // create on random port
            {});

    transport.close();

    auto result = fcntl(transport.fd(), F_GETFL);
    ASSERT_EQ(result, fcntl_error);
}

TEST(udp_server_transport, wait)
{
    auto test_data = "abcdfer";
    auto reply_data = "123456";
    auto receive_count = 0;

    framework::dispatcher dispatcher;
    framework::udp_options options;
    ipv4_udp_server_transport transport(
            dispatcher,
            options, // options
            "0.0.0.0:0", // create on random port
            [&receive_count, &reply_data](auto& source, auto& data)
            {
                receive_count++;
                source.reply(string(reply_data));
            },
            [](auto& error)
            {
                FAIL();
            });

    udp_socket_guard client_socket;
    client_socket.bind();
    client_socket.sendto(transport.at(), test_data);

    ASSERT_TRUE(transport.wait(1ms));
    ASSERT_FALSE(transport.has_pending());
}