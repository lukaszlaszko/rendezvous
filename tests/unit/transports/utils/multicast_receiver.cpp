#include "multicast_receiver.hpp"

#include <gtest/gtest.h>

#include <arpa/inet.h>
#include <cerrno>
#include <cstring>
#include <netinet/in.h>
#include <sys/socket.h>
#include <string>
#include <unistd.h>

using namespace std;


multicast_receiver::multicast_receiver(string address, uint16_t port)
    :
        address_(address),
        port_(port)
{
    socket_ = ::socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (socket_ < 0)
        ADD_FAILURE();

    sockaddr_in sin;
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = ::inet_addr(address_.c_str());
    sin.sin_port = htons(port_);
    if (::bind(socket_, reinterpret_cast<sockaddr* >(&sin), sizeof(sockaddr_in)) < 0)
        ADD_FAILURE();

    ip_mreq mref;
    mref.imr_interface = {};
    mref.imr_multiaddr = { ::inet_addr(address_.c_str()) };
    if (::setsockopt(socket_, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mref, sizeof(ip_mreq)) < 0)
        ADD_FAILURE();

    size_t sp_rcvbuf = receive_buffer_size;
    if (::setsockopt(socket_, SOL_SOCKET, SO_RCVBUF, &sp_rcvbuf, sizeof(sp_rcvbuf)) < 0)
        ADD_FAILURE();
}

multicast_receiver::~multicast_receiver()
{
    ::close(socket_);
}

bool multicast_receiver::receive()
{
    array<char, max_receive_size> buffer;
    auto receive_result = ::recv(
            socket_,
            buffer.data(),
            max_receive_size,
            MSG_DONTWAIT);

    if (receive_result < 0)
    {
        if (errno == EAGAIN)
            return false;

        ADD_FAILURE();
    }

    received_.emplace_back(buffer.data(), receive_result);
    return receive_result > 0;
}

const std::vector<std::string>& multicast_receiver::received() const
{
    return received_;
}

