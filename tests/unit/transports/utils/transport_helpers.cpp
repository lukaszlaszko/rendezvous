#include "transport_helpers.hpp"

#include <gtest/gtest.h>

#include <stdexcept>
#include <string>

#include <netinet/in.h>

using namespace std;
using namespace framework;


uint16_t find_bound_port(transport& transport)
{
    sockaddr_in sin {};

    uint32_t len_inet = sizeof(sockaddr_in);
    if (::getsockname(transport.fd(), reinterpret_cast<sockaddr*>(&sin), &len_inet) < 0)
        ADD_FAILURE();

    return ntohs(sin.sin_port);
}

void force_send(transport& transport)
{
    const auto massive_blob_size = 50 * 1024 * 1024;

    string data(massive_blob_size, 'x');
    if (::write(transport.fd(), data.data(), data.size()) >= 0)
        throw runtime_error("force_send failed");

}

