#include "tcp_socket_guard.hpp"

#include <gtest/gtest.h>


using namespace std;


tcp_socket_guard::tcp_socket_guard(string address)
    : address_(address)
{
    socket_ = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (socket_ < 0)
        ADD_FAILURE();
    
    linger lin;
    lin.l_onoff = 1;
    lin.l_linger = 1;
    if (::setsockopt(socket_, SOL_SOCKET, SO_LINGER, reinterpret_cast<void*>(&lin), sizeof(lin)) < 0)
        ADD_FAILURE();
    
    int opt = 3;
    if (::setsockopt(socket_, SOL_SOCKET, SO_RCVLOWAT, &opt,sizeof(opt)) < 0)
        ADD_FAILURE();
}

tcp_socket_guard::tcp_socket_guard(sockaddr_in& client_address, int socket)
    : 
        port_(ntohs(client_address.sin_port)),
        socket_(socket)
{
    
}

tcp_socket_guard::~tcp_socket_guard()
{
    if (socket_ > 0)
        ::close(socket_);
}

tcp_socket_guard tcp_socket_guard::connect_to(sockaddr_in sin)
{
    auto socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (socket < 0)
        ADD_FAILURE();
    
    if (::connect(socket, reinterpret_cast<sockaddr*>(&sin), sizeof(sockaddr_in)) < 0)
        ADD_FAILURE();
    
    return tcp_socket_guard(sin, socket);
}

uint16_t tcp_socket_guard::bind_and_listen()
{
    sockaddr_in sin;
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = ::inet_addr(address_.c_str());
    sin.sin_port = 0;

    if (::bind(socket_, reinterpret_cast<sockaddr *>(&sin), sizeof(sockaddr_in)) < 0)
        ADD_FAILURE();
    
    uint32_t len_inet = sizeof(sockaddr_in);
    if (::getsockname(socket_, reinterpret_cast<sockaddr*>(&sin), &len_inet) < 0)
        ADD_FAILURE();
    
    if (::listen(socket_, 1) < 0)
        ADD_FAILURE();
    
    port_ = ntohs(sin.sin_port);
    return port_;
}

tcp_socket_guard tcp_socket_guard::accept()
{
    sockaddr_in client_addr;
    socklen_t client_addr_len = sizeof(client_addr);
    auto accept_result = ::accept(
            socket_,
            reinterpret_cast<sockaddr* >(&client_addr),
            &client_addr_len);
    if (accept_result < 0)
        ADD_FAILURE();
    
    return tcp_socket_guard(client_addr, accept_result);
}

void tcp_socket_guard::force_close()
{
    if (socket_ > 0)
    {
        ::shutdown(socket_, SHUT_RDWR);
        ::close(socket_);
        socket_ = 0;
    }
}

uint16_t tcp_socket_guard::port() const
{
    return port_;
}

boost::optional<std::string> tcp_socket_guard::recv(size_t capacity)
{
    char buffer[capacity];
    auto length = ::recv(socket_, &buffer, capacity, MSG_DONTWAIT);
    if (length < 0)
        return boost::none;
    
    return string(buffer, length);
}

int tcp_socket_guard::send(string s)
{
    return ::send(socket_, s.data(), s.length(), MSG_DONTWAIT);
}

