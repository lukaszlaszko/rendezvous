#pragma once

#include <string>
#include <array>
#include <vector>


class multicast_receiver
{
public:
    multicast_receiver(std::string address, uint16_t port);
    ~multicast_receiver();

    bool receive();
    const std::vector<std::string>& received() const;

private:
    static constexpr auto max_receive_size = 2048;
    static constexpr auto receive_buffer_size = 30 * 1024;

    std::string address_;
    uint16_t port_;
    int socket_;

    std::vector<std::string> received_;
};

