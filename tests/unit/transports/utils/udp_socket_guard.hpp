#pragma once

#include <arpa/inet.h>
#include <cerrno>
#include <memory>
#include <netinet/in.h>
#include <sys/socket.h>
#include <string>

#include <boost/optional.hpp>

class udp_socket_guard final
{
public:
    udp_socket_guard(std::string address = "0.0.0.0");
    ~udp_socket_guard();
    
    udp_socket_guard(const udp_socket_guard&) = delete;
    udp_socket_guard(udp_socket_guard&&) = delete;
    udp_socket_guard& operator=(const udp_socket_guard&) = delete;
    udp_socket_guard& operator=(const udp_socket_guard&&) = delete;
    
    uint16_t bind();
    uint16_t port() const;
    
    boost::optional<std::string> recv(size_t capacity = 1024);
    int sendto(sockaddr_in destination, std::string data);
    int sendto(std::string destination, std::string data);
   
private:
    std::string address_;
    uint16_t port_{0u};
    
    int socket_;
};