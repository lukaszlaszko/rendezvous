#pragma once

#include <boost/memory.hpp>

#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <limits>


class buffer_builder
{
public:
    static const size_t remaining_length = std::numeric_limits<size_t>::max();
    
    explicit buffer_builder();
    ~buffer_builder();
    
    template <typename reference_type>
    buffer_builder& append(reference_type& reference);
    
    template <typename reference_type>
    reference_type& append_new();
    
    boost::memory::buffer_ref get(size_t offset, size_t length);
    
private:
    static const size_t initial_capacity = 2048;
    
    void* data_;
    size_t capacity_;
    size_t position_{0ul};
    
};

#include "buffer_builder.ipp"

