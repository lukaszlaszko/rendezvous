#include "udp_socket_guard.hpp"

#include <gtest/gtest.h>
#include <netinet/in.h>
#include <string>

using namespace std;


udp_socket_guard::udp_socket_guard(string address)
    : address_(address)
{
    socket_ = ::socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (socket_ < 0)
        ADD_FAILURE();
    
    linger lin;
    lin.l_onoff = 1;
    lin.l_linger = 1;
    if (::setsockopt(socket_, SOL_SOCKET, SO_LINGER, reinterpret_cast<void*>(&lin), sizeof(lin)) < 0)
        ADD_FAILURE();
    
    int opt = 3;
    if (::setsockopt(socket_, SOL_SOCKET, SO_RCVLOWAT, &opt,sizeof(opt)) < 0)
        ADD_FAILURE();
}

udp_socket_guard::~udp_socket_guard()
{
    if (socket_ > 0)
        ::close(socket_);
}

uint16_t udp_socket_guard::bind()
{
    sockaddr_in sin;
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = ::inet_addr(address_.c_str());
    sin.sin_port = 0;

    if (::bind(socket_, reinterpret_cast<sockaddr *>(&sin), sizeof(sockaddr_in)) < 0)
        ADD_FAILURE();
    
    uint32_t len_inet = sizeof(sockaddr_in);
    if (::getsockname(socket_, reinterpret_cast<sockaddr*>(&sin), &len_inet) < 0)
        ADD_FAILURE();
    
    port_ = ntohs(sin.sin_port);
    return port_;
}

uint16_t udp_socket_guard::port() const
{
    return port_;
}

boost::optional<std::string> udp_socket_guard::recv(size_t capacity)
{
    char buffer[capacity];
    auto length = ::recv(socket_, &buffer, capacity, MSG_DONTWAIT);
    if (length < 0)
        return boost::none;
    
    return string(buffer, length);
}

int udp_socket_guard::sendto(sockaddr_in destination, string data)
{
    return ::sendto(
            socket_, 
            data.data(), 
            data.length(), 
            MSG_DONTWAIT,
            reinterpret_cast<sockaddr*>(&destination),
            sizeof(sockaddr_in));
}

int udp_socket_guard::sendto(string destination, string data)
{
    auto split_position = destination.find(':');
    if (split_position == string::npos)
        ADD_FAILURE();
    
    auto address = destination.substr(0, split_position);
    auto port = destination.substr(split_position + 1, destination.length() - split_position);
    
    sockaddr_in dest_adrr {};
    dest_adrr.sin_family = AF_INET;
    inet_aton(address.data(), &dest_adrr.sin_addr);
    dest_adrr.sin_port = htons(stoul(port));
    
    return sendto(dest_adrr, data);
}

