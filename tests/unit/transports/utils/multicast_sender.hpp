#include <string>

class multicast_sender
{
public:
    multicast_sender(std::string address, uint16_t port);
    ~multicast_sender();
    
    void send(std::string data);
    
private:
    std::string address_;
    uint16_t port_;
    int socket_;
};

