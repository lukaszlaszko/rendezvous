#pragma once

#include <arpa/inet.h>
#include <cerrno>
#include <memory>
#include <netinet/in.h>
#include <sys/socket.h>
#include <string>

#include <boost/optional.hpp>

class tcp_socket_guard final
{
public:
    tcp_socket_guard(std::string address = "0.0.0.0");
    tcp_socket_guard(tcp_socket_guard&&) = default;
    ~tcp_socket_guard();
    
    tcp_socket_guard(const tcp_socket_guard&) = delete;
    tcp_socket_guard& operator=(const tcp_socket_guard&) = delete;
    tcp_socket_guard& operator=(const tcp_socket_guard&&) = delete;
    
    static tcp_socket_guard connect_to(sockaddr_in sin);
    
    uint16_t bind_and_listen();
    tcp_socket_guard accept();
    void force_close();
    
    uint16_t port() const;
    
    boost::optional<std::string> recv(size_t capacity = 1024);
    int send(std::string s);
   
private:
    tcp_socket_guard(sockaddr_in& client_address, int socket);
    
    std::string address_;
    uint16_t port_{0u};
    
    int socket_;
};