#pragma once

#include <algorithm>
#include <cerrno>
#include <cstdlib>
#include <system_error>
#include <type_traits>


inline buffer_builder::buffer_builder()
{
    data_ = std::malloc(initial_capacity);
    if (data_ == nullptr)
        throw std::system_error(
                std::error_code(errno, std::generic_category()));
    capacity_ = initial_capacity;
}

inline buffer_builder::~buffer_builder()
{
    if (data_ != nullptr)
    {
        std::free(data_);
    }
}

template <typename reference_type>
inline buffer_builder& buffer_builder::append(reference_type& reference)
{
    auto reference_size = sizeof(reference);
    if (capacity_ - position_ < reference_size)
    {
        auto new_capacity = capacity_ * 2ul;
        auto new_data = std::realloc(data_, new_capacity);
        if (new_data == nullptr)
            throw std::system_error(
                std::error_code(errno, std::generic_category()));
        
        data_ = new_data;
        capacity_ = new_capacity;
    }
    
    using non_reference_type = typename std::remove_reference<reference_type>::type;
    using pointer_type = typename std::add_pointer<non_reference_type>::type;
    *reinterpret_cast<pointer_type>(reinterpret_cast<uint8_t*>(data_) + position_) = reference;
    position_ += reference_size;
    
    return *this;
}

template <typename reference_type>
inline reference_type& buffer_builder::append_new()
{
    auto reference_size = sizeof(reference_type);
    if (capacity_ - position_ < reference_size)
    {
        auto new_capacity = capacity_ * 2ul;
        auto new_data = std::realloc(data_, new_capacity);
        if (new_data == nullptr)
            throw std::system_error(
                std::error_code(errno, std::generic_category()));
        
        data_ = new_data;
        capacity_ = new_capacity;
    }
    
    using non_reference_type = typename std::remove_reference<reference_type>::type;
    using pointer_type = typename std::add_pointer<non_reference_type>::type;
    auto& new_reference = 
            *reinterpret_cast<pointer_type>(
                    reinterpret_cast<uint8_t*>(data_) + position_);
    position_ += reference_size;
    
    return new_reference;
}

inline boost::memory::buffer_ref buffer_builder::get(size_t offset, size_t length)
{
    auto beginning = reinterpret_cast<uint8_t*>(data_);
    auto real_length = std::min(length, position_ - offset);
    return boost::memory::buffer_ref(beginning + offset, real_length);
}

