#include "multicast_sender.hpp"

#include <gtest/gtest.h>

#include <arpa/inet.h>
#include <cerrno>
#include <cstring>
#include <netinet/in.h>
#include <sys/socket.h>
#include <string>
#include <unistd.h>

using namespace std;


multicast_sender::multicast_sender(string address, uint16_t port)
    : 
        address_(address), 
        port_(port)
{
    socket_ = ::socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (socket_ < 0)
        ADD_FAILURE();

    uint8_t opt = 1;
    if (::setsockopt(socket_, IPPROTO_IP, IP_MULTICAST_TTL, &opt, sizeof(opt)) < 0)
        ADD_FAILURE();
    
    opt = 1;
    if (::setsockopt(socket_, IPPROTO_IP, IP_MULTICAST_LOOP, &opt, sizeof(opt)) < 0)
        ADD_FAILURE();
}

multicast_sender::~multicast_sender()
{
    ::close(socket_);
}

void multicast_sender::send(string data)
{
    sockaddr_in sin;
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = ::inet_addr(address_.c_str());
    sin.sin_port = htons(port_);

    auto send_result = ::sendto(
            socket_, 
            data.data(), 
            data.length(), 
            0, 
            reinterpret_cast<sockaddr *>(&sin),sizeof(sin));
    if (send_result < 0)
        FAIL();
}

