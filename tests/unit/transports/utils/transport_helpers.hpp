#pragma once

#include <framework/transports/transport.hpp>

#include <cstdio>


uint16_t find_bound_port(framework::transport& transport);

void force_send(framework::transport& transport);