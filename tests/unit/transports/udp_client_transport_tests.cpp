#include "utils/udp_socket_guard.hpp"
#include "utils/transport_helpers.hpp"

#include <framework/dispatcher.hpp>
#include <framework/transports/network/ipv4.hpp>
#include <framework/transports/udp_client_transport.hpp>
#include <syscall_simulator/syscall_simulator.hpp>

#include <boost/concurrency.hpp>
#include <boost/memory.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <array>
#include <string>

using namespace std;
using namespace boost::memory;
using namespace boost::concurrency;

using ipv4_traits = framework::ip::v4::traits;
using spsc_ring_buffer = spsc_variable_circular_buffer<>;
using ipv4_udp_client_transport = framework::udp_client_transport<ipv4_traits, spsc_ring_buffer>;


/**
 * Tests creation of a single udp_client_transport.
 */
TEST(udp_client_transport, create)
{
    udp_socket_guard server_socket;
    server_socket.bind();

    framework::dispatcher dispatcher;
    ipv4_udp_client_transport transport(
            dispatcher,
            {}, // default options
            "127.0.0.1:" + to_string(server_socket.port()),
            {});

    ASSERT_EQ(transport.info().from(), "127.0.0.1:" + to_string(server_socket.port()));
}

/**
 * Tests creation of a multiple udp_client_transports, bound to the same server endpoint.
 */
TEST(udp_client_transport, create_multiple)
{
    udp_socket_guard server_socket;
    server_socket.bind();

    framework::dispatcher dispatcher;
    ipv4_udp_client_transport transport_1(
            dispatcher,
            {}, // options
            "127.0.0.1:" + to_string(server_socket.port()),
            {});

    ipv4_udp_client_transport transport_2(
            dispatcher,
            {}, // options
            "127.0.0.1:" + to_string(server_socket.port()),
            {});

    ASSERT_EQ(transport_1.info().from(), "127.0.0.1:" + to_string(server_socket.port()));
    ASSERT_EQ(transport_2.info().from(), "127.0.0.1:" + to_string(server_socket.port()));
}

/**
 * Tests sending referencable data
 */
TEST(udp_client_transport, send_referenceable)
{
    auto test_data = "abcderfg";

    udp_socket_guard server_socket;
    server_socket.bind();

    framework::dispatcher dispatcher;
    ipv4_udp_client_transport transport(
            dispatcher,
            {}, // options
            "127.0.0.1:" + to_string(server_socket.port()),
            {},
            [](auto& error)
            {
                FAIL();
            });

    string test_string(test_data);
    transport.send(test_string);

    dispatcher.dispatch();
    auto received_data = server_socket.recv();
    ASSERT_NE(received_data, boost::none);
    ASSERT_EQ(*received_data, test_data);
}

/**
 * Tests sending an rvalue object.
 */
TEST(udp_client_transport, send_rvalue)
{
    auto test_data = "abcderfg";

    udp_socket_guard server_socket;
    server_socket.bind();

    framework::dispatcher dispatcher;
    ipv4_udp_client_transport transport(
            dispatcher,
            {}, // options
            "127.0.0.1:" + to_string(server_socket.port()),
            {},
            [](auto& error)
            {
                FAIL();
            });

    transport.send(string(test_data));

    auto received_data = server_socket.recv();
    ASSERT_NE(received_data, boost::none);
    ASSERT_EQ(*received_data, test_data);
}

TEST(udp_client_transport, receive_registered_source)
{
    auto test_data = "abcde";
    auto received_count = 0;
    string received_data;

    udp_socket_guard server_socket;
    server_socket.bind();

    framework::dispatcher dispatcher;
    ipv4_udp_client_transport transport(
            dispatcher,
            {}, // options
            "127.0.0.1:" + to_string(server_socket.port()),
            [&received_count, &received_data](auto& source, buffer_ref& data)
            {
                received_count++;
                received_data.clear();
                received_data.append(data.as_pointer<char*>(), data.length());
            },
            [](auto& error)
            {
                FAIL();
            });

    auto transport_port = find_bound_port(transport);
    server_socket.sendto("127.0.0.1:" + to_string(transport_port), test_data);

    ASSERT_EQ(received_count, 0);

    dispatcher.dispatch();
    ASSERT_EQ(received_count, 1);
    ASSERT_EQ(received_data, test_data);
}

TEST(udp_client_transport, receive_wrong_source)
{
    auto test_data = "abcde";
    auto received_count = 0;

    udp_socket_guard server_socket_1;
    server_socket_1.bind();

    udp_socket_guard server_socket_2;
    server_socket_2.bind();

    framework::dispatcher dispatcher;
    ipv4_udp_client_transport transport(
            dispatcher,
            {}, // options
            "127.0.0.1:" + to_string(server_socket_1.port()),
            [&received_count](auto& source, buffer_ref& data)
            {
                received_count++;
            },
            [](auto& error)
            {
                FAIL();
            });

    auto transport_port = find_bound_port(transport);
    server_socket_2.sendto("127.0.0.1:" + to_string(transport_port), test_data);

    ASSERT_EQ(received_count, 0);

    dispatcher.dispatch();
    ASSERT_EQ(received_count, 0);
}

TEST(udp_client_transport, close)
{
    static const auto fcntl_error = -1;

    udp_socket_guard server_socket;
    server_socket.bind();

    framework::dispatcher dispatcher;
    ipv4_udp_client_transport transport(
            dispatcher,
            {}, // default options
            "127.0.0.1:" + to_string(server_socket.port()),
            {});

    transport.close();

    auto result = fcntl(transport.fd(), F_GETFL);
    ASSERT_EQ(result, fcntl_error);
}

TEST(udp_client_transport, wait)
{
    auto test_data = "abcderfg";

    udp_socket_guard server_socket;
    server_socket.bind();

    framework::dispatcher dispatcher;
    ipv4_udp_client_transport transport(
            dispatcher,
            {}, // options
            "10.0.0.1:" + to_string(server_socket.port()),
            {},
            [](auto& error)
            {
                FAIL();
            });

    {
        syscall_interceptor sendto_guard(SYS_sendto, [](long& result)
        {
            result = -EAGAIN;
            return true;
        });

        ASSERT_TRUE(transport.wait(1ms));

        array<char, 1000> data;
        generate_n(data.begin(), data.size(), rand);
        transport.send(buffer_ref(data.data(), data.size()));

        ASSERT_TRUE(transport.has_pending());
        ASSERT_FALSE(transport.wait(1ms));
        ASSERT_TRUE(transport.has_pending());
    }

    force_send(transport);
    this_thread::sleep_for(100ms);

    ASSERT_TRUE(dispatcher.dispatch());
    ASSERT_FALSE(transport.has_pending());
}