#include "utils/multicast_receiver.hpp"

#include <framework/dispatcher.hpp>
#include <framework/transports/network/ipv4.hpp>
#include <framework/transports/udp_publisher_transport.hpp>
#include <syscall_simulator/syscall_simulator.hpp>

#include <boost/concurrency.hpp>
#include <boost/memory.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <algorithm>
#include <array>
#include <iostream>
#include <thread>

using namespace std;
using namespace testing;
using namespace boost::memory;

using ipv4_traits = framework::ip::v4::traits;
using spsc_ring_buffer = boost::concurrency::spsc_variable_circular_buffer<>;
using ipv4_udp_publisher_transport = framework::udp_publisher_transport<ipv4_traits, spsc_ring_buffer>;


/**
 * Tests creation of a udp_publisher_transport
 */
TEST(udp_publisher_transport, create)
{
    framework::dispatcher dispatcher;
    ipv4_udp_publisher_transport transport(
            dispatcher,
            {}, // options
            "224.0.0.1:40000",
            "0.0.0.0");
}

/**
 * Tests sending data when buffering in internal ring buffer isnt necessary.
 */
TEST(udp_publisher_transport, send__no_buffer)
{
    multicast_receiver receiver("224.0.0.1", 40000);

    framework::dispatcher dispatcher;
    ipv4_udp_publisher_transport transport(
            dispatcher,
            {}, // options
            "224.0.0.1:40000",
            "0.0.0.0",
            {});

    transport.send(string("mnb56"));

    dispatcher.dispatch();
    ASSERT_TRUE(receiver.receive());
    ASSERT_EQ(receiver.received().size(), 1ul);
    ASSERT_EQ(receiver.received().back(), "mnb56");
}

TEST(udp_publisher_transport, send__with_buffer)
{
    multicast_receiver receiver("224.0.0.1", 40000);

    framework::dispatcher dispatcher;
    framework::udp_options options;
    options.sp_sndbuf = 1024;

    ipv4_udp_publisher_transport transport(
            dispatcher,
            options,
            "224.0.0.1:40000",
            "0.0.0.0",
            [](auto& error)
            {
                FAIL();
            });

    syscall_interceptor sendto_guard(SYS_sendto, [](long& result)
    {
        result = -EAGAIN;
        return true;
    });

    array<char, 1000> data;
    generate_n(data.begin(), data.size(), rand);
    transport.send(buffer_ref(data.data(), data.size()));
    ASSERT_TRUE(transport.has_pending());

    this_thread::sleep_for(100ms);

    while(receiver.receive());
    ASSERT_LT(receiver.received().size(), data.size());

    ASSERT_TRUE(dispatcher.dispatch());
    ASSERT_TRUE(receiver.receive());
}

TEST(udp_publisher_transport, close)
{
    static const auto fcntl_error = -1;

    framework::dispatcher dispatcher;
    ipv4_udp_publisher_transport transport(
            dispatcher,
            {}, // options
            "224.0.0.1:40000",
            "0.0.0.0",
            {});

    transport.close();

    auto result = fcntl(transport.fd(), F_GETFL);
    ASSERT_EQ(result, fcntl_error);
}

TEST(udp_publisher_transport, wait_no_pending)
{
    framework::dispatcher dispatcher;
    ipv4_udp_publisher_transport transport(
            dispatcher,
            {}, // options
            "224.0.0.1:40000",
            "0.0.0.0",
            {});

    ASSERT_TRUE(transport.wait(500ms));
}

TEST(udp_publisher_transport, wait_pending_timeout)
{
    framework::dispatcher dispatcher;
    ipv4_udp_publisher_transport transport(
            dispatcher,
            {}, // options
            "224.0.0.1:40000",
            "0.0.0.0",
            [](auto& error)
            {
                FAIL();
            });

    syscall_interceptor sendto_guard(
            SYS_sendto,
            [](long& result)
            {
                result = -EAGAIN;
                return true;
            });

    array<char, 1000> data;
    generate_n(data.begin(), data.size(), rand);
    transport.send(buffer_ref(data.data(), data.size()));

    ASSERT_TRUE(transport.has_pending());
    ASSERT_FALSE(transport.wait(1ms));
    ASSERT_TRUE(transport.has_pending());
}
