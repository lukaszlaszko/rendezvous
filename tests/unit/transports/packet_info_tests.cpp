#include <framework/transports/network/ipv4.hpp>
#include <framework/transports/packet_info.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <chrono>
#include <ctime>

using namespace std;
using namespace std::chrono;
using namespace testing;

using ipv4_traits = framework::ip::v4::traits;
using ipv4_packet_info = framework::packet_info<ipv4_traits>;


TEST(packet_info, since_no_time_set)
{
    ipv4_packet_info info;
    
    nanoseconds since_receive;
    nanoseconds since_read;
    tie(since_receive, since_read) = info.since();
    
    ASSERT_EQ(since_receive, 0ns);
    ASSERT_EQ(since_read, 0ns);
}

TEST(packet_info, receive_time_not_set)
{
    ipv4_packet_info info;
    
    auto receive_time = info.receive_time();
    ASSERT_EQ(receive_time, system_clock::time_point(nanoseconds::zero()));
}

TEST(buffer_ref_with_info, receive_time)
{
    ipv4_packet_info info;
    
    timespec ts { 1ul, 100ul };
    info.data().receive_time = ts;
    
    auto receive_time = info.receive_time();
    auto expected_time = system_clock::time_point(
            seconds{ts.tv_sec} 
          + nanoseconds{ts.tv_nsec});
    ASSERT_EQ(receive_time, expected_time);
}

TEST(packet_info, read_time_not_set)
{
    ipv4_packet_info info;
    
    auto read_time = info.read_time();
    ASSERT_EQ(read_time, system_clock::time_point(nanoseconds::zero()));
}

TEST(packet_info, read_time)
{
    ipv4_packet_info info;
    
    timespec ts { 1ul, 100ul };
    info.data().read_time = ts;
    
    auto read_time = info.read_time();
    auto expected_time = system_clock::time_point(
            seconds{ts.tv_sec} 
          + nanoseconds{ts.tv_nsec});
    ASSERT_EQ(read_time, expected_time);
}

TEST(packet_info, since_receive_time_set)
{
    ipv4_packet_info info;
    
    timespec ts { 1ul, 100ul };
    info.data().receive_time = ts;
    
    nanoseconds since_receive;
    nanoseconds since_read;
    tie(since_receive, since_read) = info.since();
    
    ASSERT_GT(since_receive, 0ns);
    ASSERT_EQ(since_read, 0ns);
}

TEST(packet_info, since_read_time_set)
{
    ipv4_packet_info info;
    
    timespec ts { 1ul, 100ul };
    info.data().read_time = ts;
    
    nanoseconds since_receive;
    nanoseconds since_read;
    tie(since_receive, since_read) = info.since();
    
    ASSERT_EQ(since_receive, 0ns);
    ASSERT_GT(since_read, 0ns);
}

