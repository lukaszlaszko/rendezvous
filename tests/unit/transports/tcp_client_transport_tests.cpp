#include "utils/tcp_socket_guard.hpp"

#include <framework/dispatcher.hpp>
#include <framework/transports/network/ipv4.hpp>
#include <framework/transports/tcp_client_transport.hpp>
#include <syscall_simulator/syscall_simulator.hpp>

#include <boost/concurrency.hpp>
#include <boost/memory.hpp>
#include <boost/optional.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <array>
#include <cerrno>
#include <list>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/socket.h>
#include <sstream>
#include <string>
#include <system_error>
#include <thread>

using namespace std;
using namespace boost::memory;
using namespace testing;

using ipv4_traits = framework::ip::v4::traits;
using spsc_ring_buffer = boost::concurrency::spsc_variable_circular_buffer<>;
using ipv4_tcp_client_transport = framework::tcp_client_transport<ipv4_traits, spsc_ring_buffer>;


TEST(tcp_client_transport, create_connection_refused)
{
    list<error_code> errors;

    framework::dispatcher dispatcher;
    ipv4_tcp_client_transport transport(
            dispatcher,
            {}, // options
            "127.0.0.1:34000",
            [](auto& source)
            {
                FAIL();
            }, // connected callback
            [](auto& source)
            {
                FAIL();
            }, // disconnected callback
            [](auto& source, auto& buffer)
            {
                FAIL();
            },
            [&errors](auto& error)
            {
                errors.push_back(error.code());
            });

    ASSERT_EQ(errors.size(), 0ul);
    ASSERT_FALSE(transport.is_connected());

    ASSERT_TRUE(dispatcher.dispatch(500ms));
    ASSERT_EQ(errors.size(), 1ul);
    ASSERT_EQ(errors.back().category(), generic_category());
    ASSERT_EQ(errors.back().value(), static_cast<int>(errc::connection_refused));
    ASSERT_FALSE(transport.is_connected());
}

TEST(tcp_client_transport, create_connection_accepted)
{
    auto connected_count = 0;
    auto disconnected_count = 0;
    list<error_code> errors;

    tcp_socket_guard server_socket;
    server_socket.bind_and_listen();

    framework::dispatcher dispatcher;
    ipv4_tcp_client_transport transport(
            dispatcher,
            {}, // options
            "127.0.0.1:" + to_string(server_socket.port()),
            [&connected_count](auto& source)
            {
                connected_count++;
            }, // connected callback
            [&disconnected_count](auto& source)
            {
                disconnected_count++;
            }, // disconnected callback
            [](auto& source, auto& buffer)
            {
            },
            [&errors](auto& error)
            {
                cerr << error << endl;
                errors.push_back(error.code());
            });

    ASSERT_EQ(connected_count, 0u);
    ASSERT_EQ(disconnected_count, 0u);
    ASSERT_EQ(errors.size(), 0ul);
    ASSERT_FALSE(transport.is_connected());

    ASSERT_TRUE(dispatcher.dispatch());

    auto client_socket = server_socket.accept();

    ASSERT_EQ(connected_count, 1);
    ASSERT_EQ(disconnected_count, 0);
    ASSERT_EQ(errors.size(), 0ul);
    ASSERT_TRUE(transport.is_connected());
}

TEST(tcp_client_transport, create_connection_by_dns_accepted)
{
    auto connected_count = 0u;
    auto disconnected_count = 0u;
    list<error_code> errors;

    tcp_socket_guard server_socket;
    server_socket.bind_and_listen();

    framework::dispatcher dispatcher;
    ipv4_tcp_client_transport transport(
            dispatcher,
            {}, // options
            "localhost:" + to_string(server_socket.port()),
            [&connected_count](auto& source)
            {
                connected_count++;
            }, // connected callback
            [&disconnected_count](auto& source)
            {
                disconnected_count++;
            }, // disconnected callback
            [](auto& source, auto& buffer)
            {
            },
            [&errors](auto& error)
            {
                cerr << error << endl;
                errors.push_back(error.code());
            });

    ASSERT_EQ(connected_count, 0u);
    ASSERT_EQ(disconnected_count, 0u);
    ASSERT_EQ(errors.size(), 0ul);
    ASSERT_FALSE(transport.is_connected());

    ASSERT_TRUE(dispatcher.dispatch());

    auto client_socket = server_socket.accept();

    ASSERT_EQ(connected_count, 1u);
    ASSERT_EQ(disconnected_count, 0u);
    ASSERT_EQ(errors.size(), 0ul);
    ASSERT_TRUE(transport.is_connected());
}

TEST(tcp_client_transport, disconnected_by_remote)
{
    auto connected_count = 0u;
    auto disconnected_count = 0u;
    list<error_code> errors;

    tcp_socket_guard server_socket;
    server_socket.bind_and_listen();

    framework::dispatcher dispatcher;
    ipv4_tcp_client_transport transport(
            dispatcher,
            {}, // options
            "127.0.0.1:" + to_string(server_socket.port()),
            [&connected_count](auto& source)
            {
                connected_count++;
            }, // connected callback
            [&disconnected_count](auto& source)
            {
                disconnected_count++;
            }, // disconnected callback
            [](auto& source, auto& buffer)
            {
            },
            [&errors](auto& error)
            {
                cerr << error << endl;
                errors.push_back(error.code());
            });

    ASSERT_EQ(connected_count, 0u);
    ASSERT_EQ(disconnected_count, 0u);
    ASSERT_EQ(errors.size(), 0ul);
    ASSERT_FALSE(transport.is_connected());

    ASSERT_TRUE(dispatcher.dispatch());

    auto client_socket = server_socket.accept();

    ASSERT_EQ(connected_count, 1u);
    ASSERT_EQ(disconnected_count, 0u);
    ASSERT_EQ(errors.size(), 0ul);
    ASSERT_TRUE(transport.is_connected());

    client_socket.force_close();

    ASSERT_EQ(connected_count, 1u);
    ASSERT_EQ(disconnected_count, 0u);
    ASSERT_EQ(errors.size(), 0ul);

    this_thread::sleep_for(300ms);
    dispatcher.dispatch();

    ASSERT_EQ(connected_count, 1u);
    ASSERT_EQ(disconnected_count, 1u);
    ASSERT_EQ(errors.size(), 0ul);
}

TEST(tcp_client_test, send_on_disconnected_by_remote)
{
    list<error_code> errors;

    tcp_socket_guard server_socket;
    server_socket.bind_and_listen();

    framework::dispatcher dispatcher;
    ipv4_tcp_client_transport transport(
            dispatcher,
            {}, // options
            "127.0.0.1:" + to_string(server_socket.port()),
            { }, // connected callback
            { }, // disconnected callback
            [](auto& source, auto& buffer)
            {
            },
            [&errors](auto& error)
            {
                errors.push_back(error.code());
            });

    dispatcher.dispatch();
    auto client_socket = server_socket.accept();

    ASSERT_TRUE(transport.is_connected());

    client_socket.force_close();
    dispatcher.dispatch();
    ASSERT_FALSE(transport.is_connected());
    transport.send(string("abc"));

    ASSERT_FALSE(errors.empty());
    ASSERT_EQ(errors.back(), errc::broken_pipe);
}

//TEST(tcp_client_test, send_with_callback)
//{
//    tcp_socket_guard server_socket;
//    server_socket.bind_and_listen();
//
//    framework::dispatcher dispatcher;
//    ipv4_tcp_client_transport transport(
//            dispatcher,
//            {}, // options
//            "127.0.0.1:" + to_string(server_socket.port()),
//            { }, // connected callback
//            { }, // disconnected callback
//            [](auto& source, auto& buffer)
//            {
//            },
//            [](auto& error)
//            {
//                cerr << error << endl;
//                FAIL();
//            });
//
//    dispatcher.dispatch();
//    auto client_socket = server_socket.accept();
//
//    ASSERT_TRUE(transport.is_connected());
//
//    string s("abc");
//    transport.send([&s](auto slot)
//    {
//        memcpy(slot, s.data(), s.length());
//    }, s.length());
//
//    auto received = client_socket.recv();
//    ASSERT_EQ(received, boost::none);
//
//    dispatcher.dispatch();
//
//    received = client_socket.recv();
//    ASSERT_NE(received, boost::none);
//    ASSERT_EQ(*received, s);
//}

TEST(tcp_client_test, close)
{
    static const auto fcntl_error = -1;

    tcp_socket_guard server_socket;
    server_socket.bind_and_listen();

    framework::dispatcher dispatcher;
    ipv4_tcp_client_transport transport(
            dispatcher,
            {}, // options
            "127.0.0.1:" + to_string(server_socket.port()),
            { }, // connected callback
            { }, // disconnected callback
            [](auto& source, auto& buffer)
            {
            },
            [](auto& error)
            {
                cerr << error << endl;
                FAIL();
            });

    transport.close();

    auto result = fcntl(transport.fd(), F_GETFL);
    ASSERT_EQ(result, fcntl_error);
}

TEST(tcp_client_test, send_with_reference)
{
    tcp_socket_guard server_socket;
    server_socket.bind_and_listen();

    framework::dispatcher dispatcher;
    ipv4_tcp_client_transport transport(
            dispatcher,
            {}, // options
            "127.0.0.1:" + to_string(server_socket.port()),
            { }, // connected callback
            { }, // disconnected callback
            [](auto& source, auto& buffer)
            {
            },
            [](auto& error)
            {
                cerr << error << endl;
                FAIL();
            });

    dispatcher.dispatch();
    auto client_socket = server_socket.accept();

    ASSERT_TRUE(transport.is_connected());

    string s("abc");
    transport.send(s);

    auto received = client_socket.recv();
    ASSERT_NE(received, boost::none);
    ASSERT_EQ(*received, s);
}

TEST(tcp_client_test, send_with_rvalue)
{
    tcp_socket_guard server_socket;
    server_socket.bind_and_listen();

    framework::dispatcher dispatcher;
    ipv4_tcp_client_transport transport(
            dispatcher,
            {}, // options
            "127.0.0.1:" + to_string(server_socket.port()),
            { }, // connected callback
            { }, // disconnected callback
            [](auto& source, auto& buffer)
            {
            },
            [](auto& error)
            {
                cerr << error << endl;
                FAIL();
            });

    dispatcher.dispatch();
    auto client_socket = server_socket.accept();

    ASSERT_TRUE(transport.is_connected());

    string s("abc");
    transport.send(move(s));

    auto received = client_socket.recv();
    ASSERT_NE(received, boost::none);
    ASSERT_EQ(*received, s);
}

TEST(tcp_client_test, send_data_too_large)
{
    tcp_socket_guard server_socket;
    server_socket.bind_and_listen();

    framework::dispatcher dispatcher;
    framework::tcp_options options;
    ipv4_tcp_client_transport transport(
            dispatcher,
            options, // options
            "127.0.0.1:" + to_string(server_socket.port()),
            { }, // connected callback
            { }, // disconnected callback
            [](auto& source, auto& buffer)
            {
            },
            [](auto& error)
            {
                cerr << error << endl;
                FAIL();
            });

    dispatcher.dispatch();
    auto client_socket = server_socket.accept();
    ASSERT_TRUE(transport.is_connected());

    syscall_interceptor sendto_guard(
            SYS_sendto,
            [](long& result)
            {
                result = -EAGAIN;
                return true;
            });

    string data(options.sp_sndbuf * 10, 'x');
    ASSERT_FALSE(transport.send(buffer_ref(data.data(), data.size())));
}

TEST(tcp_client_test, wait_for_send)
{
    tcp_socket_guard server_socket;
    server_socket.bind_and_listen();

    framework::dispatcher dispatcher;
    framework::tcp_options options;
    options.sp_sndbuf = 1024;

    ipv4_tcp_client_transport transport(
            dispatcher,
            options, // options
            "127.0.0.1:" + to_string(server_socket.port()),
            { }, // connected callback
            { }, // disconnected callback
            [](auto& source, auto& buffer)
            {
            },
            [](auto& error)
            {
                cerr << error << endl;
                FAIL();
            });

    array<char, 1000> data;
    generate_n(data.begin(), data.size(), rand);

    dispatcher.dispatch();
    auto client_socket = server_socket.accept();

    ASSERT_TRUE(transport.is_connected());

    for (auto i = 0; i < 1000; i++)
        transport.send(buffer_ref(data.data(), data.size()));
    ASSERT_FALSE(transport.wait(1ms));

    transport.wait(1s);

    dispatcher.dispatch();
    ASSERT_TRUE(transport.wait(1ms));
}
