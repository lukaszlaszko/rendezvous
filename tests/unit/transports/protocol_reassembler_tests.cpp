#include "utils/buffer_builder.hpp"

#include <framework/transports/protocol_reassembler.hpp>

#include <boost/memory.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

using namespace std;
using namespace testing;

namespace fixed_size_protocol {
    
enum class message_type : uint8_t
{
    message_1,
    message_2
};

struct message_1
{
    
};

struct message_2
{
    
};

struct message
{
    message_type type;
    union
    {
        message_1 msg1;
        message_2 msg2;
    };
};

struct protocol_traits
{
    static size_t complete_size(boost::memory::buffer_ref&& data)
    {
        return sizeof(message);
    }
};
    
}

namespace variable_size_protocol {

enum class message_type : char
{
    msg1 = '1',
    msg2 = '2',
    msg3 = '3'
};

struct message_header
{
    message_type type_;
};

struct msg1
{
    int field1;
    char field2[12];
};

struct msg2
{
    double field1;
    size_t field2;
};

struct msg3
{
    double field1;
    double field2;
    double field3;
};

struct message
{
    message(message_type type)
        : header({ type })
    { }
    
    message_header header;
    union
    {
        msg1 m1;
        msg2 m2;
        msg3 m3;
    };
};

struct protocol_traits
{
    inline static size_t complete_size(boost::memory::buffer_ref&& first)
    {
        auto& header = first.as<message_header&>();
        switch (header.type_)
        {
            case message_type::msg1:
                return sizeof(msg1);
            case message_type::msg2:
                return sizeof(msg2);
            case message_type::msg3:
                return sizeof(msg3);
            default:
                throw std::logic_error("unknown message type");
        }
    }
};

}

static auto source = 1;

TEST(protocol_reassembler, complete_single_message__fixed_size_protocol)
{
    using reassembler_type = 
            framework::protocol_reassembler<
                    decltype(source), 
                    fixed_size_protocol::protocol_traits>;
    
    auto message_count = 0;
    reassembler_type reassembler([&message_count](auto& source, auto& data)
    {
        message_count++;
    });
    
    buffer_builder buffer;
    buffer.append_new<fixed_size_protocol::message>();
    
    ASSERT_EQ(message_count, 0);
    
    reassembler(source, buffer.get(0, buffer_builder::remaining_length));
    ASSERT_EQ(message_count, 1);
    ASSERT_FALSE(reassembler.unfinished());
}

TEST(protocol_reassembler, complete_mulitple_messages__fixed_size_protocol)
{
    using reassembler_type = 
            framework::protocol_reassembler<
                    decltype(source), 
                    fixed_size_protocol::protocol_traits>;
    
    auto message_count = 0;
    reassembler_type reassembler([&message_count](auto& source, auto& data)
    {
        message_count++;
    });
    
    buffer_builder buffer;
    buffer.append_new<fixed_size_protocol::message>();
    buffer.append_new<fixed_size_protocol::message>();
    buffer.append_new<fixed_size_protocol::message>();
    
    ASSERT_EQ(message_count, 0);
    
    reassembler(source, buffer.get(0, buffer_builder::remaining_length));
    ASSERT_EQ(message_count, 3);
    ASSERT_FALSE(reassembler.unfinished());
}

TEST(protocol_reassembler, split_message__fixed_size_protocol)
{
    using reassembler_type = 
            framework::protocol_reassembler<
                    decltype(source), 
                    fixed_size_protocol::protocol_traits>;
    
    auto message_count = 0;
    reassembler_type reassembler([&message_count](auto& source, auto& data)
    {
        message_count++;
    });
    
    buffer_builder buffer;
    buffer.append_new<fixed_size_protocol::message>();
    buffer.append_new<fixed_size_protocol::message>();
    buffer.append_new<fixed_size_protocol::message>();
    
    ASSERT_EQ(message_count, 0);
    ASSERT_FALSE(reassembler.unfinished());
    
    reassembler(source, buffer.get(0, sizeof(fixed_size_protocol::message) - 1));
    ASSERT_EQ(message_count, 0);
    
    reassembler(source, buffer.get(sizeof(fixed_size_protocol::message) - 1, buffer_builder::remaining_length));
    ASSERT_EQ(message_count, 3);
    ASSERT_FALSE(reassembler.unfinished());
}

TEST(protocol_reassembler, split_message_unfinished__fixed_size_protocol)
{
    using reassembler_type = 
            framework::protocol_reassembler<
                    decltype(source), 
                    fixed_size_protocol::protocol_traits>;
    
    auto message_count = 0;
    reassembler_type reassembler([&message_count](auto& source, auto& data)
    {
        message_count++;
    });
    
    buffer_builder buffer;
    buffer.append_new<fixed_size_protocol::message>();
    
    ASSERT_EQ(message_count, 0);
    ASSERT_FALSE(reassembler.unfinished());
    
    reassembler(source, buffer.get(0, sizeof(fixed_size_protocol::message) - 1));
    
    ASSERT_EQ(message_count, 0);
    ASSERT_TRUE(reassembler.unfinished());
}

TEST(protocol_reassembler, partial_message__variable_size_protocol)
{
    using reassembler_type = 
            framework::protocol_reassembler<
                    decltype(source), 
                    variable_size_protocol::protocol_traits>;
    
    auto message_count = 0;
    reassembler_type reassembler([&message_count](auto& source, auto& data)
    {
        message_count++;
    });
    
    variable_size_protocol::message msg(variable_size_protocol::message_type::msg1);
    
    buffer_builder buffer;
    buffer.append(msg);
    
    reassembler(source, buffer.get(0, sizeof(variable_size_protocol::msg1) - 2));
    ASSERT_EQ(message_count, 0);
    
    reassembler(source, buffer.get(sizeof(variable_size_protocol::msg1) - 2, 2));
    ASSERT_EQ(message_count, 1);
}

TEST(protocol_reassembler, full_message__variable_size_protocol)
{
    using reassembler_type = 
            framework::protocol_reassembler<
                    decltype(source), 
                    variable_size_protocol::protocol_traits>;
    
    auto message_count = 0;
    reassembler_type reassembler([&message_count](auto& source, auto& data)
    {
        message_count++;
    });
    
    variable_size_protocol::message msg(variable_size_protocol::message_type::msg1);
    
    buffer_builder buffer;
    buffer.append(msg);
    
    reassembler(source, buffer.get(0, sizeof(variable_size_protocol::msg1)));
    ASSERT_EQ(message_count, 1);
}

TEST(protocol_reassembler, partial_limit_reached)
{
    const size_t unfinished = 0ul;
    struct infinite_message_trait
    {
        static size_t complete_size(boost::memory::buffer_ref&& data)
        {
            return unfinished;
        }
    };
    
    char block[8192];
    boost::memory::buffer_ref block_ref(block, sizeof(block));
    
    using reassembler_type = 
            framework::protocol_reassembler<
                    decltype(source), 
                    infinite_message_trait,
                    sizeof(block) / 2>;
    
    reassembler_type reassembler([](auto& source, auto& data)
    {
        FAIL();
    });
            
    ASSERT_THROW(reassembler(source, move(block_ref)), std::length_error);
}

