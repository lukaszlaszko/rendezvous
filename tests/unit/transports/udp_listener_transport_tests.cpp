#include "utils/multicast_sender.hpp"

#include <framework/dispatcher.hpp>
#include <framework/transports/network/ipv4.hpp>
#include <framework/transports/udp_listener_transport.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <chrono>
#include <iostream>

using namespace std;
using namespace std::chrono;
using namespace testing;

using ipv4_traits = framework::ip::v4::traits;
using ipv4_udp_listener_transport = framework::udp_listener_transport<ipv4_traits>;

TEST(udp_listener_transport, create_with_single_group)
{
    framework::dispatcher dispatcher;
    ipv4_udp_listener_transport transport(
            dispatcher,
            { }, // options,
            "224.0.0.1:30000", // group,
            "0.0.0.0", // interface
            [](auto& source, auto& data) { }, // receive callback,
            [](auto& error) { });
            
    cout << transport << endl;
}

TEST(udp_listener_transport, create_with_multiple_groups)
{
    framework::dispatcher dispatcher;
    ipv4_udp_listener_transport transport(
            dispatcher,
            { }, // options,
            { "224.0.0.1", "224.0.0.2" }, // groups,
            30000, // port
            "0.0.0.0", // interface
            [](auto& source, auto& data) { }, // receive callback,
            [](auto& error) { });
            
    cout << transport << endl;
}

TEST(udp_listener_transport, listen_single_group)
{
    auto received_count = 0;
    
    framework::dispatcher dispatcher;
    ipv4_udp_listener_transport transport(
            dispatcher,
            { }, // options,
            "224.0.0.1:30000", // group,
            "0.0.0.0", // interface
            [&received_count](auto& source, boost::memory::buffer_ref& data) 
            {
                received_count++;
                string received(data.as_pointer<char*>(), data.length());
                ASSERT_EQ(received, "abc");
                ASSERT_EQ(source.info().from(), "224.0.0.1:30000");
            }, // receive callback,
            [](auto& error) { });
            
    multicast_sender sender("224.0.0.1", 30000);
    sender.send("abc");
    ASSERT_EQ(received_count, 0);
    
    dispatcher.dispatch();
    ASSERT_EQ(received_count, 1);
}

TEST(udp_listener_transport, listen_two_groups)
{
    auto received_count = 0;
    
    framework::dispatcher dispatcher;
    ipv4_udp_listener_transport transport(
            dispatcher,
            { }, // options,
            { "224.0.0.1", "224.0.0.2" }, // groups,
            30000, // port
            "0.0.0.0", // interface
            [&received_count](auto& source, auto& buffer) 
            {
                received_count++;
            }, // receive callback,
            [](auto& error) { });
            
    multicast_sender sender_one("224.0.0.1", 30000);
    sender_one.send("abc");
    ASSERT_EQ(received_count, 0);
    
    dispatcher.dispatch();
    ASSERT_EQ(received_count, 1);
    
    multicast_sender sender_two("224.0.0.2", 30000);
    sender_two.send("abc");
    
    dispatcher.dispatch();
    ASSERT_EQ(received_count, 2);
}

TEST(udp_listener_transport, listen_two_groups_one_filtered)
{
    auto received_count = 0;
    ipv4_udp_listener_transport::endpoint_type received_from("0.0.0.0:0");
    
    framework::dispatcher dispatcher;
    ipv4_udp_listener_transport transport(
            dispatcher,
            { }, // options,
            { "224.0.0.1", "224.0.0.2" }, // groups,
            30000, // port
            "0.0.0.0", // interface
            [&received_count, &received_from](auto& source, auto& data) 
            {
                received_count++;
                received_from = source.info().from();
            }, // receive callback,
            [](auto& error) { });
            
    multicast_sender sender_one("224.0.0.1", 30000);
    sender_one.send("abc");
    ASSERT_EQ(received_count, 0);
    
    dispatcher.dispatch();
    ASSERT_EQ(received_count, 1);
    ASSERT_EQ(received_from, "224.0.0.1:30000");
    
    multicast_sender sender_two("224.0.0.2", 30000);
    sender_two.send("abc");
    
    dispatcher.dispatch();
    ASSERT_EQ(received_count, 2);
    ASSERT_EQ(received_from, "224.0.0.2:30000");
    
    multicast_sender sender_three("224.0.0.3", 30000);
    sender_three.send("abc");
    
    dispatcher.dispatch();
    ASSERT_EQ(received_count, 2);
}

TEST(udp_listener_transport, listen_pktinfo_disabled)
{
    auto received_count = 0;
    
    framework::udp_options options;
    options.pktinfo = false;
    
    framework::dispatcher dispatcher;
    ipv4_udp_listener_transport transport(
            dispatcher,
            options,
            "224.0.0.1:30000", // group,
            "0.0.0.0", // interface
            [&received_count](auto& source, auto& buffer) 
            {
                received_count++;
                ASSERT_EQ(source.info().from(), "0.0.0.0:0");
            }, // receive callback,
            [](auto& error) { });
            
    multicast_sender sender("224.0.0.1", 30000);
    sender.send("abc");
    ASSERT_EQ(received_count, 0);
    
    dispatcher.dispatch();
    ASSERT_EQ(received_count, 1);
}

TEST(udp_listener_transport, listen_timestampns_disabled)
{
    auto received_count = 0;
    
    framework::udp_options options;
    options.timestampns = false;
    
    framework::dispatcher dispatcher;
    ipv4_udp_listener_transport transport(
            dispatcher,
            options,
            "224.0.0.1:30000", // group,
            "0.0.0.0", // interface
            [&received_count](auto& source, auto& buffer) 
            {
                received_count++;
                ASSERT_EQ(source.info().receive_time(), system_clock::time_point(0ns));
            }, // receive callback,
            [](auto& error) { });
            
    multicast_sender sender("224.0.0.1", 30000);
    sender.send("abc");
    ASSERT_EQ(received_count, 0);
    
    dispatcher.dispatch();
    ASSERT_EQ(received_count, 1);
}

TEST(udp_listener_transport, close)
{
    static const auto fcntl_error = -1;

    framework::dispatcher dispatcher;
    ipv4_udp_listener_transport transport(
            dispatcher,
            { }, // options,
            "224.0.0.1:30000", // group,
            "0.0.0.0", // interface
            [](auto& source, auto& data) { }, // receive callback,
            [](auto& error) { });

    transport.close();

    auto result = fcntl(transport.fd(), F_GETFL);
    ASSERT_EQ(result, fcntl_error);
}

