#include <framework/dispatchable.hpp>
#include <framework/dispatcher.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <unistd.h>

using namespace std;
using namespace testing;


class mock_dispatchable : protected framework::dispatchable
{
public:
    mock_dispatchable(
            int fd,
            bool can_read,
            bool can_write,
            framework::dispatcher& dispatcher,
            error_callback_type error_callback = {})
            : framework::dispatchable(error_callback)
    {
        framework::dispatchable::set_fd(fd);
        dispatcher.add(*this, can_read, can_write, false, false);
    }

    MOCK_METHOD3(handle_event, void(
            bool should_receive,
            bool should_send,
            bool should_disconnect));
};


TEST(dispatcher, create)
{
    framework::dispatcher dispatcher;
}

TEST(dispatcher, create_with_dependent)
{
    framework::dispatcher parent;
    framework::dispatcher child(parent);
}

TEST(dispatcher, dispatch_read)
{
    framework::dispatcher disp;

    int pipefd[2];
    ASSERT_EQ(pipe(pipefd), 0);

    string s("abc");
    EXPECT_GT(write(pipefd[1], s.data(), s.length()), 0);

    mock_dispatchable dispatchable(pipefd[0], true, false, disp);
    EXPECT_CALL(dispatchable, handle_event(Eq(true), Eq(false), Eq(false)))
            .Times(1);

    EXPECT_TRUE(disp.dispatch());

    close(pipefd[0]);
    close(pipefd[1]);
}

TEST(dispatcher, dispatch_write)
{
    framework::dispatcher disp;

    int pipefd[2];
    ASSERT_EQ(pipe(pipefd), 0);

    mock_dispatchable dispatchable(pipefd[1], false, true, disp);
    EXPECT_CALL(dispatchable, handle_event(Eq(false), Eq(true), Eq(false)))
            .Times(1);

    EXPECT_TRUE(disp.dispatch());

    close(pipefd[0]);
    close(pipefd[1]);
}

TEST(dispatcher, dispatch_timeout)
{
    framework::dispatcher disp;

    ASSERT_FALSE(disp.dispatch(100ms));
}

TEST(dispatcher, dispatch_dependant)
{
    framework::dispatcher parent;
    framework::dispatcher child(parent);

    int pipefd[2];
    ASSERT_EQ(pipe(pipefd), 0);

    string s("abc");
    EXPECT_GT(write(pipefd[1], s.data(), s.length()), 0);

    mock_dispatchable dispatchable(pipefd[0], true, false, child);
    EXPECT_CALL(dispatchable, handle_event(Eq(true), Eq(false), Eq(false)))
            .Times(1);

    EXPECT_TRUE(parent.dispatch());

    close(pipefd[0]);
    close(pipefd[1]);
}

TEST(dispatcher, call_soon__lambda)
{
    framework::dispatcher dispatcher;

    auto called = false;
    dispatcher.call_soon([&](){ called = true; });
    ASSERT_FALSE(called);

    auto dispatched = dispatcher.dispatch();
    ASSERT_TRUE(dispatched);
    ASSERT_TRUE(called);
}

TEST(dispatcher, call_soon__more_than_max_events)
{
    framework::dispatcher dispatcher;

    auto called_1 = false;
    auto called_2 = false;
    dispatcher.call_soon([&](){ called_1 = true; });
    dispatcher.call_soon([&](){ called_2 = true; });
    ASSERT_FALSE(called_1);
    ASSERT_FALSE(called_2);

    ASSERT_TRUE(dispatcher.dispatch<1>());
    ASSERT_TRUE(called_1);
    ASSERT_FALSE(called_2);

    ASSERT_TRUE(dispatcher.dispatch<1>());
    ASSERT_TRUE(called_1);
    ASSERT_TRUE(called_2);
}

TEST(dispatcher, call_soon__dispatch_with_parent)
{
    framework::dispatcher parent;
    framework::dispatcher child(parent);

    auto called = false;
    child.call_soon([&](){ called = true; });
    ASSERT_FALSE(called);

    auto dispatched = parent.dispatch();
    ASSERT_TRUE(dispatched);
    ASSERT_TRUE(called);
}
