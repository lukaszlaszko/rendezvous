#include <framework/dispatcher.hpp>
#include <framework/loop.hpp>
#include <framework/timers.hpp>

#include <chrono>
#include <exception>
#include <iostream>

using namespace std;


int main(int argc, char** argv) 
{
    framework::dispatcher dispatcher;
    framework::loop main_loop(dispatcher);
    
    framework::one_shot_timer timer(
            dispatcher,
            [&main_loop](framework::one_shot_timer& source)
            {
                main_loop.interrupt();
            },
            [](exception& error)
            {
                cerr << error.what() << endl;
            });
    timer.schedule(20s);
    
    cout << "started at " << endl;
    main_loop.run_forever();
    cout << "finished at " << endl;
    
    return 0;
}

