#include <framework/dispatcher.hpp>
#include <framework/loop.hpp>
#include <framework/timers.hpp>

#include <chrono>
#include <exception>
#include <iostream>

using namespace std;


int main(int argc, char** argv) 
{
    framework::dispatcher dispatcher;
    framework::loop main_loop(dispatcher);
    
    framework::auto_reload_timer timer(
            dispatcher,
            30s,
            [](framework::auto_reload_timer& source, uint64_t expirations)
            {
                cout << "trigger with " << expirations << " expirations!" << endl;
            },
            [](exception& error)
            {
                cerr << error.what() << endl;
            });
    timer.arm();
    
    main_loop.interrupt_on(SIGKILL);
    main_loop.run_forever();
    
    return 0;
}

