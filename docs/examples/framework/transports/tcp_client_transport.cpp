#include <framework/dispatcher.hpp>
#include <framework/loop.hpp>
#include <framework/transports.hpp>

#include <iostream>

using namespace std;

int main(int argc, char** argv) 
{
    framework::dispatcher dispatcher;
    framework::loop main_loop(dispatcher);

    framework::tcp_options options;
    framework::ipv4_tcp_client::single_sender::client_transport transport(
            dispatcher,
            options,
            "time.nist.gov:13",
            [](auto& source)
            {
                cout << "connected!" << endl;
            },
            [](auto& source)
            {
                cout << "disconnected!" << endl;
            },
            [](auto& source, auto& buffer)
            {
                cout << buffer.length() << endl;
            },
            [](auto& error)
            {
                cerr << error.what() << endl;
            });
    
    main_loop.interrupt_on(SIGKILL);
    main_loop.run_forever();
    
    return 0;
}

