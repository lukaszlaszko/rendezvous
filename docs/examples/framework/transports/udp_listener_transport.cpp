#include <framework/dispatcher.hpp>
#include <framework/loop.hpp>
#include <framework/transports.hpp>

#include <iostream>

using namespace std;

int main(int argc, char** argv) 
{
    framework::dispatcher dispatcher;
    framework::loop main_loop(dispatcher);

    framework::udp_options options;
    framework::ipv4_udp::listener_transport transport(
            dispatcher,
            options,
            "224.0.0.100:32000",
            "0.0.0.0",
            [](auto& source, auto& buffer)
            {
                auto& info = source.info();
                cerr << "received " << buffer.length() << " bytes from " << info.from() << endl;
            },
            [](auto& error)
            {
                cerr << error.what() << endl;
            });

    main_loop.interrupt_on(SIGKILL);
    main_loop.run_forever();
    
    return 0;
}
