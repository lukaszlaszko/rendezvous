#include <framework/dispatcher.hpp>
#include <framework/loop.hpp>
#include <framework/transports.hpp>

#include <iostream>

using namespace std;

int main(int argc, char** argv) 
{
    framework::dispatcher dispatcher;
    framework::loop main_loop(dispatcher);

    framework::tcp_options options;
    framework::ipv4_tcp_server::single_sender::server_transport transport(
            dispatcher,
            options,
            "0.0.0.0:27000",
            [](auto& connected_client)
            {
                cout << connected_client << " connected!" << endl;
            },
            [](auto& disconnected_client)
            {
                cout << disconnected_client << " disconnected!" << endl;
            },
            [](auto& connected_client, auto& buffer)
            {
                cout << "received " << buffer.length() << " bytes from " << connected_client << endl;
            },
            [](auto& error)
            {
                cerr << error.what() << endl;
            });
    
    main_loop.interrupt_on(SIGKILL);
    main_loop.run_forever();
    
    return 0;
}
