#include <framework/dispatcher.hpp>
#include <framework/loop.hpp>
#include <framework/transports.hpp>

#include <boost/memory.hpp>

#include <iostream>
#include <string>

using namespace std;
using namespace boost::memory;

int main(int argc, char** argv) 
{
    framework::dispatcher dispatcher;
    framework::loop main_loop(dispatcher);

    framework::udp_options options;
    framework::ipv4_udp::single_sender::client_transport transport(
            dispatcher,
            options,
            "time.nist.gov:123",
            [](auto& source, buffer_ref& buffer)
            {
                string content(buffer.as_pointer<char*>(), buffer.length());
                cout << content << endl;
            },
            [](auto& error)
            {
                cerr << error.what() << endl;
            });
            
    struct ntp_request
    {
        const uint8_t version_mode{0x1B};
        const uint8_t reserved[47]{};
    };
    
    ntp_request request;
    transport.send(&request);
    
    main_loop.interrupt_on(SIGKILL);
    main_loop.run_forever();
    
    return 0;
}

