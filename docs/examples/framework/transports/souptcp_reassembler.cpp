#include <framework/dispatcher.hpp>
#include <framework/loop.hpp>
#include <framework/transports.hpp>
#include <framework/transports/protocol_reassembler.hpp>

#include <boost/memory.hpp>

#include <iostream>

using namespace std;

struct soup_header
{
    uint16_t packet_length_;
    char packet_type_;
};

struct debug : soup_header
{
    static const char identifier = '+';
    
    char text_[0];
};

struct login_accepted : soup_header
{
    static const char identifier = 'A';
    
    char session_[10];
    char sequence_number_[20];
};

struct login_rejected : soup_header
{
    static const char identifier = 'J';
    
    char reject_reason_code_;
};

struct sequenced_data : soup_header
{
    static const char identifier = 'S';
    
    char message_[0];
};

struct souptcp_traits
{
    static const size_t unfinished = 0ul;
    
    inline static size_t complete_size(boost::memory::buffer_ref&& buffer)
    {
        if (buffer.length() < sizeof(soup_header))
            return unfinished;
        
        auto& header = buffer.as<soup_header&>();
        return header.packet_length_;
    }
};

int main(int argc, char** argv) 
{
    framework::dispatcher dispatcher;
    framework::loop main_loop(dispatcher);

    framework::tcp_options options;
    framework::ipv4_tcp_client::single_sender::client_transport transport(
            dispatcher,
            options,
            "203.18.165.65:17810",
            [](auto& source)
            {
                cout << "connected!" << endl;
            },
            [](auto& source)
            {
                cout << "disconnected!" << endl;
            },
            framework::protocol_reassembler<decltype(transport), souptcp_traits>(
                    [](auto& source, boost::memory::buffer_ref& buffer)
                    {
                        auto& header = buffer.as<soup_header&>();
                        cout << "message:" << header.packet_type_ << " length:" << buffer.length() << endl;
                    }),
            [](auto& error)
            {
                cerr << error.what() << endl;
            });
    
    main_loop.interrupt_on(SIGKILL);
    main_loop.run_forever();
    
    return 0;
}

