#include <framework/dispatcher.hpp>
#include <framework/loop.hpp>
#include <framework/transports.hpp>

#include <iostream>

using namespace std;

int main(int argc, char** argv) 
{
    framework::dispatcher dispatcher;
    framework::loop main_loop(dispatcher);

    framework::udp_options options;
    framework::ipv4_udp::single_sender::server_transport transport(
            dispatcher,
            options,
            "0.0.0.0:27000",
            [](auto& source, auto& buffer)
            {
                cout << "received " << buffer.length() << " bytes from " << source << endl;
            },
            [](auto& error)
            {
                cerr << error.what() << endl;
            });
    
    main_loop.interrupt_on(SIGKILL);
    main_loop.run_forever();
    
    return 0;
}
