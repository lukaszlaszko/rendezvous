#include <framework/dispatcher.hpp>
#include <framework/loop.hpp>
#include <framework/timers.hpp>
#include <framework/transports.hpp>

#include <iostream>
#include <string>

using namespace std;

int main(int argc, char** argv) 
{
    framework::dispatcher dispatcher;
    framework::loop main_loop(dispatcher);

    framework::udp_options options;
    framework::ipv4_udp::single_sender::publisher_transport transport(
            dispatcher,
            options,
            "224.0.0.1:27000",
            "0.0.0.0",
            [](auto& error)
            {
                cerr << error.what() << endl;
            });
            
    framework::auto_reload_timer timer(
            dispatcher,
            30s,
            [&transport](framework::auto_reload_timer& source, uint64_t expirations)
            {
                transport.send("trigger with " + to_string(expirations) + " expirations!");
            },
            [](exception& error)
            {
                cerr << error.what() << endl;
            });
    timer.arm();
    
    main_loop.interrupt_on(SIGKILL);
    main_loop.run_forever();
    
    return 0;
}
