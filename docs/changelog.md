## Changelog

# 1.0 (28 February 2017)

* Initial release
    * signal_handler
    * auto_reload_timer
    * one_shot_timer
    * transports
        * ipv4
        * udp_client_transport
        * udp_server_transport
        * tcp_client_transport
        * tcp_server_transport
        * udp_publisher_transport
        * udp_listener_transport