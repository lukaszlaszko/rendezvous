[![Download](https://api.bintray.com/packages/lukaszlaszko/shadow/rendezvous%3Ashadow/images/download.svg) ](https://bintray.com/lukaszlaszko/shadow/rendezvous%3Ashadow/_latestVersion)
[![Sources](https://img.shields.io/badge/bitbucket-sources-green.svg?style=flat)](https://bitbucket.org/lukaszlaszko/rendezvous/)
[![codecov](https://codecov.io/bb/lukaszlaszko/rendezvous/branch/master/graph/badge.svg)](https://codecov.io/bb/lukaszlaszko/rendezvous)
[![Documentation](https://img.shields.io/badge/bitbucket-documentation-blue.svg?style=flat)](http://lukaszlaszko.bitbucket.io/rendezvous.git/master)
## Table of contents

* [Introduction](#markdown-header-introduction)
* [Components](#markdown-header-components)
    * [Dispatcher](#markdown-header-dispatcher)
    * [Loop](#markdown-header-loop)
    * [Signal handler](#markdown-header-signal-handler)
    * [Timers](#markdown-header-timers)
    * [Transports](#markdown-header-transports)
    * [Reactors](#markdown-header-reactors)
* [How to use](#markdown-header-how-to-use)
* [How to build](#markdown-header-how-to-build)


---
## Introduction

A library providing infrastructure for writing applications around event loop(s). Modern event loops provide means for scalable dispatch of events. Events coming from a range of communication transports and system related facilities. This intends to simplify the way how applications are written, enabling them to be better composed and scaled. 

This library aims at defining user-minimalistic, feature-complex C++ 14 layer between user code and facilities such as signal handlers, timers, network transports. All components have been designed to favour RAII model.

## Components

The library is build around a concept of dispatching events from queues. Those queues are native utilities designed to deliver ordered notifications about action required on file descriptors. Depending on platform following I/O facilities are used:

* Linux - [epoll](http://man7.org/linux/man-pages/man7/epoll.7.html)
* BSD - [kqueue/kevent](https://www.freebsd.org/cgi/man.cgi?query=kqueue&sektion=2) [not supported]
* Windows - [I/O completion ports](https://msdn.microsoft.com/en-us/library/windows/desktop/aa365198(v=vs.85).aspx) [not supported]

Currently only Linux with epoll is supported.

![general_objects.png](https://bitbucket.org/repo/KjpnXL/images/1479975714-general_objects.png)

### Dispatcher

I/O event dispatch facility is abstracted within the library by dispatcher. A dispatcher may aggregate multiple dispatchables and is intended to deliver notifications in ordered manner only to dispatchables registered with it. Notifications can be dispatched to to dispatchables only one at the time enabling concurrent reactive code to be run single-threaded.

![dispatcher.png](https://bitbucket.org/repo/KjpnXL/images/3192250727-dispatcher.png)

### Loop

It is a central execution device provided by the framework. Provides a declarative interface for interruptable dispatch loop on referenced **dispatcher**. 

The loop provides following facilities:

* dispatch loop invoked through a call to **run_forever**.
* direct interruption of dispatch loop through **interrupt**.
* indirect interruption of dispatch loop through single handlers.

As such **loop** is intended to represent an execution point of a thread polling from a dispatcher or a hierarchy of dispatchers.

For example:

    #include <framework/dispatcher.hpp>
    #include <framework/loop.hpp> 

    ...

    framework::dispatcher dispatcher;
    framework::loop main_loop(dispatcher);

    ...

    main_loop.interrupt_on(SIGTERM);
    main_loop.interrupt_on(SIGKILL);
    main_loop.interrupt_on(SIGINT);
    main_loop.run_forever();

### Signal handler

An async handler for POSIX reliable signals. Using the handler enables dispatch of signal callback among callback triggered by other dispatchables.

For example:

    #include <framework/dispatcher.hpp>
    #include <framework/signals/signal_handler.hpp>

    ...

    framework::dispatcher dispatcher;
    framework::signal_handler handler(
            dispatcher, 
            SIGUSR1, 
            [](auto signo)
            {
                cout << "signal " << signo << " received" << endl;
            },
            [](auto& error)
            {
                cerr << error.what() << endl;
            });
    ...
    


### Timers

Timers are used to schedule callback invocation at certain moment in future. Two different timer types are provided:

* **One shot timer**

    Triggers a callback at the first possible dispatch after given scheduled delay. Delay is specified as [std::chrono::duration](http://en.cppreference.com/w/cpp/chrono/duration):

        #include <framework/timers.hpp>

        ...

        framework::dispatcher dispatcher;
        framework::one_shot_timer timer(
                dispatcher,
                [&main_loop](framework::one_shot_timer& source)
                {
                    cout << "timer triggered" << endl;
                },
                [](exception& error)
                {
                    cerr << error.what() << endl;
                });
        timer.schedule(20s);

        ...

* **Auto reload timer**

    Triggers a callback periodically with fixed interval given as [std::chrono::duration](http://en.cppreference.com/w/cpp/chrono/duration):

        #include <framework/dispatcher.hpp>
        #include <framework/timers.hpp>

        ...

        framework::dispatcher dispatcher;
        framework::auto_reload_timer timer(
                dispatcher,
                30s,
                [](framework::auto_reload_timer& source, uint64_t expirations)
                {
                    cout << "trigger with " << expirations << " expirations!" << endl;
                },
                [](exception& error)
                {
                    cerr << error.what() << endl;
                });
        timer.arm(); 

        ...

To use timers include **framework/timers.hpp** header.

### Transports

* [TCP protocol](https://en.wikipedia.org/wiki/Transmission_Control_Protocol) is supported on both server and client sides. Each unique client - server stream is represented by a transport. Both client and server transports can be optimised with options in **framework::tcp_options**.

    * **TCP server**

        Listens for connections from TCP network clients. Create a tcp server in the following way:

            #include <framework/dispatcher.hpp>
            #include <framework/loop.hpp>
            #include <framework/transports.hpp>

            ...

            framework::dispatcher dispatcher;
            framework::tcp_options options;
            framework::ipv4_tcp_server::single_sender::server_transport transport(
                    dispatcher,
                    options,
                    "0.0.0.0:27000",
                    [](auto& connected_client)
                    {
                        cout << connected_client << " connected!" << endl;
                    },
                    [](auto& disconnected_client)
                    {
                        cout << disconnected_client << " disconnected!" << endl;
                    },
                    [](auto& connected_client, auto& buffer)
                    {
                        cout << "received " << buffer.length() << " bytes from " << connected_client << endl;
                    },
                    [](auto& error)
                    {
                        cerr << error.what() << endl;
                    });

            ...

        Whenever a connection is accepted, a new client transport is created for that connection. This is a self-standing entity serving uniquely connection with connected client. Multiple client transport may exist on the course tcp server lifetime. However client transport can't supersede lifespan of it's server. 

        The server begins listening and accepting incoming connections immediately after its creation. All client connections are forcibly closed when their server is destroyed.

        A reference to served connection is provided to each of registered callbacks. These references can be used outside of these callbacks, however it has to be ensured that they aren't used after server is destroyed.

    * **TCP client**

        Provides client connection for TCP network services. For example:

            #include <framework/dispatcher.hpp>
            #include <framework/loop.hpp>
            #include <framework/transports.hpp>

            ...
        

            framework::dispatcher dispatcher;
            framework::loop main_loop(dispatcher);

            framework::tcp_options options;
            framework::ipv4_tcp_client::single_sender::client_transport transport(
                    dispatcher,
                    options,
                    "time.nist.gov:13",
                    [](auto& source)
                    {
                        cout << "connected!" << endl;
                    },
                    [](auto& source)
                    {
                        cout << "disconnected!" << endl;
                    },
                    [](auto& source, auto& buffer)
                    {
                        cout << buffer.length() << endl;
                    },
                    [](auto& error)
                    {
                        cerr << error.what() << endl;
                    });
    
            main_loop.interrupt_on(SIGKILL);
            main_loop.run_forever();

            ...

        For more details look into **framework::tcp_client_transport** type documentation.

* [UDP protocol](https://en.wikipedia.org/wiki/User_Datagram_Protocol) support is divided into server and client components. The first accepts incoming datagrams and is capable to reply with datagrams, the second adds support for sending inbound requests to UDP servers. Both client and server transports can be optimised with options in **framework::udp_options**. 

    * **UDP server**

        A UDP unicast server. This transport has a capability for binding to a local UDP endpoint, listening for incoming datagrams and replying to them. Usage example:

            #include <framework/dispatcher.hpp>
            #include <framework/loop.hpp>
            #include <framework/transports.hpp>

            ...

            framework::dispatcher dispatcher;
            framework::loop main_loop(dispatcher);

            framework::udp_options options;
            framework::ipv4_udp::single_sender::server_transport transport(
                dispatcher,
                options,
                "0.0.0.0:25000",
                [](auto& source, buffer_ref& data)
                {
                    cerr << "received " << buffer.length() << " bytes from " << source << endl;

                    source.reply(string("accepted"));
                },
                [](auto& error)
                {
                    cout << error.what() << endl;
                });

            main_loop.interrupt_on(SIGKILL);
            main_loop.run_forever();

            ...

        For more details look into **framework::udp_server_transport** type documentation.

    * **UDP client**

        A UDP unicast client. This transports provides methods for sending datagrams to a remote endpoint in a non blocking way and receiving replays. Usage example:
  
            #include <framework/dispatcher.hpp>
            #include <framework/loop.hpp>
            #include <framework/transports.hpp>

            ...

            framework::dispatcher dispatcher;
            framework::loop main_loop(dispatcher);

            framework::udp_options options;
            framework::ipv4_udp::single_sender::client_transport transport(
                dispatcher,
                options,
                "192.168.30.60:25000",
                [](auto& source, buffer_ref& data)
                {
                    cerr << "received " << buffer.length() << " bytes from " << source << endl;
                },
                [](auto& error)
                {
                    cout << error.what() << endl;
                });

            transport.send(string("test request"));

            main_loop.interrupt_on(SIGKILL);
            main_loop.run_forever();

            ...

        For more details look into **framework::udp_client_transport** type documentation.

* [IP multicast](https://en.wikipedia.org/wiki/IP_multicast) with [UDP](https://en.wikipedia.org/wiki/User_Datagram_Protocol) is supported by separate publisher and listener transports. Usage of both transports can be optimised through adjustment of **framework::udp_options**. 

    * **UDP multicast publisher**

        Sends UDP datagrams to a single IP multicast group. For example:

            #include <framework/dispatcher.hpp>
            #include <framework/loop.hpp>
            #include <framework/transports.hpp>

            ...

            framework::dispatcher dispatcher;
            framework::loop main_loop(dispatcher);

            framework::udp_options options;
            framework::ipv4_udp::single_sender::publisher_transport transport(
                    dispatcher,
                    options,
                    "224.0.0.1:27000",
                    "0.0.0.0",
                    [](auto& error)
                    {
                        cerr << error.what() << endl;
                    });
            
            framework::auto_reload_timer timer(
                   dispatcher,
                   30s,
                   [&transport](framework::auto_reload_timer& source, uint64_t expirations)
                   {
                       transport.send("trigger with " + to_string(expirations) + " expirations!");
                   },
                   [](exception& error)
                   {
                       cerr << error.what() << endl;
                   });
            timer.arm();

            ...

        For more details look into **framework::udp_publisher_transport** type documentation.

    * **UDP multicast listener**

        A UDP receiver for IP multicast. This transport has a capability for listening to multiple multicast groups with the same UDP port. Usage example:

            #include <framework/dispatcher.hpp>
            #include <framework/loop.hpp>
            #include <framework/transports.hpp>

            ...

            framework::dispatcher dispatcher;
            framework::loop main_loop(dispatcher);

            framework::udp_options options;
            framework::ipv4_udp::listener_transport transport(
                    dispatcher,
                    options,
                    "224.0.0.100:32000",
                    "0.0.0.0",
                    [](auto& source, auto& buffer)
                    {
                        cerr << "received " << buffer.length() << " bytes from " << source << endl;
                    },
                    [](auto& error)
                    {
                        cerr << error.what() << endl;
                    });

            main_loop.interrupt_on(SIGKILL);
            main_loop.run_forever();

            ...

        For more details look into **framework::udp_listener_transport** type documentation.

    You can learn more about IP/UDP multicast from [here](http://www.tldp.org/HOWTO/Multicast-HOWTO-2.html).    

### Reactors

Reactors are components which can be registered as handlers to callbacks produced by other components. it's implementation of [reactive programming pattern](https://en.wikipedia.org/wiki/Reactive_programming) where components are connected into reactive chains. Each of the components reacting to triggers produced by other components, transforming trigger notifications and delivering transformed output further in the chain. 

Following reactive components are provided as a part of this framework

* **Protocol reassembler**  

    Reconstruct application level protocol messages from receive callbacks produced by transports. Triggers a callback once a complete protocol message is reassembled. Usage example:

        #include <framework/transports/protocol_reassembler.hpp>

        ...

        struct soup_header
        {
            uint16_t packet_length_;
            char packet_type_;
        };

        struct debug : soup_header
        {
            static const char identifier = '+';
    
            char text_[0];
        };

        struct login_accepted : soup_header
        {
            static const char identifier = 'A';
    
            char session_[10];
            char sequence_number_[20];
        };

        struct login_rejected : soup_header
        {
            static const char identifier = 'J';
    
            char reject_reason_code_;
        };

        struct sequenced_data : soup_header
        {
            static const char identifier = 'S';
    
            char message_[0];
        };

        struct souptcp_traits
        {
            static const size_t unfinished = 0ul;
    
            inline static size_t complete_size(framework::buffer_ref&& buffer)
            {
                if (buffer.length() < sizeof(soup_header))
                    return unfinished;
        
                auto& header = buffer.as<soup_header&>();
                return header.packet_length_;
            }
        };

        ...

        framework::dispatcher dispatcher;

        framework::tcp_options options;
        framework::ipv4_tcp_client::single_sender::client_transport transport(
                dispatcher,
                options,
                "203.18.165.65:17810",
                [](auto& source)
                {
                    cout << "connected!" << endl;
                },
                [](auto& source)
                {
                    cout << "disconnected!" << endl;
                },
                framework::protocol_reassembler<decltype(transport), souptcp_traits>(
                        [](auto& source, framework::buffer_ref& buffer)
                        {
                            auto& header = buffer.as<soup_header&>();
                            cout << "message:" << header.packet_type_ << " length:" << buffer.length() << endl;
                        }),
                [](auto& error)
                {
                    cerr << error.what() << endl;
                });

        ...

## How to use

The code has been written in compliance with C++ 14. Either from the following compilers should be used:

* GCC 5.0 or newer
* Clang 3.4 or newer

This library is purely header based. As such it doesn't need to be separately compiled before being used. However all dependecies have to be referenced alongside with the library within used build system. For example in [CMake](https://cmake.org) with [ExternalProject](https://cmake.org/cmake/help/v3.2/module/ExternalProject.html):

* **CMakeLists.txt**

    In project's root CMakeLists.txt file:

        cmake_minimum_required(VERSION 3.2)
        ...
        include(ExternalProject)
        ...
        set(CMAKE_CXX_STANDARD 14)
        add_definitions(-Wno-unknown-attributes)
        ...
        add_subdirectory(external)

* **external/CMakeLists.txt** 

    In projects external reference definition file: 

        ExternalProject_Add(allocators
            URL https://bitbucket.org/lukaszlaszko/allocators/get/1.1.zip
            BUILD_COMMAND ""
            INSTALL_COMMAND "")

        ExternalProject_Get_Property(allocators SOURCE_DIR)
        Set(ALLOCATORS_BUFFERS_INCLUDE_DIRS ${SOURCE_DIR}/src/include PARENT_SCOPE)

        ExternalProject_Add(circular_buffers
            URL https://bitbucket.org/lukaszlaszko/circular_buffers/get/1.1.zip
            BUILD_COMMAND ""
            INSTALL_COMMAND "")

        ExternalProject_Get_Property(circular_buffers SOURCE_DIR)
        Set(CIRCULAR_BUFFERS_INCLUDE_DIRS ${SOURCE_DIR}/src/include PARENT_SCOPE)

        ExternalProject_Add(rendezvous
            URL https://bitbucket.org/lukaszlaszko/rendezvous/get/1.1.zip
            BUILD_COMMAND ""
            INSTALL_COMMAND "")

        ExternalProject_Get_Property(rendezvous SOURCE_DIR)
        Set(RENDEZVOUS_INCLUDE_DIRS ${SOURCE_DIR}/src/include PARENT_SCOPE)
        
    This will leave CMake **PARENT_SCOPE** variable scope with 3 variables pointing at include directories of which should be added to include path during compialtion:
    
    - **ALLOCATORS_BUFFERS_INCLUDE_DIRS** - include path of [allocators dependency](https://bitbucket.org/lukaszlaszko/allocators)
    - **CIRCULAR_BUFFERS_INCLUDE_DIRS** - include path of [circular buffers dependency](https://bitbucket.org/lukaszlaszko/circular_buffers)
    - **RENDEZVOUS_INCLUDE_DIRS** - libray's include path 
    
    In this example **PARENT_SCOPE** of **external/CMakeLists.txt** is identical with the root scope. As such the variables
    will be available at all scopes. The scripts adds dependencies as compilation targets, which registered as dependecies will automatically download and preprocess dependencies as needed.

* **src/CMakeLists.txt**

        ...
        include_directories(${ALLOCATORS_BUFFERS_INCLUDE_DIRS})
        include_directories(${CIRCULAR_BUFFERS_INCLUDE_DIRS})
        include_directories(${RENDEZVOUS_INCLUDE_DIRS})     
        ...
        
    This adds dependencies include paths to include paths considered during compilation.

* **src/sometarget/CMakeLists.txt**

        add_library(sometarget MODULE
            main.cpp)

        add_dependencies(sometarget 
            allocators 
            circular_buffers 
            rendezvous)
            
    **add_dependencies** to targets registered with **external/CMakeLists.txt**. This will trigger libaries download into temporary location
    and population of include paths when a build on dependent target is requested.
    
Once build system is configured, client code may reference headers from **<rendezvous>/src/include**:

* **framework/dispatcher.hpp** - for [dispatcher](#markdown-header-dispatcher).
* **framework/loop.hpp** - for main [loop](#markdown-header-loop).
* **framework/timers.hpp** - for [timers](#markdown-header-dispatcher-timers).
* **framework/transports.hpp** - for communication [transports](#markdown-header-transports).

For example:
    
    #include <framework/dispatcher.hpp>
    #include <framework/loop.hpp>
    #include <framework/timers.hpp>
    #include <framework/transports.hpp>

    #include <iostream>
    #include <string>

    using namespace std;

    int main(int argc, char** argv)
    {
    ...

## How to build

In order to build samples and unit tests, following pre-requisites have to be provided:

* Linux 2.7 or newer
* C++ 14 compatible compiler - clang 3.9 is recommended
* Cmake 3.2 or newer

Build steps:

1. Clone source repository

        $ git clone https://bitbucket.org/lukaszlaszko/rendezvous.git

2. Initialise submodules

        $ git submodule update --init 

3. Create build directory

        $ mkdir bin


4. Generate build configurations

        $ cd bin
        $ cmake ..


5. Compile

        $ cmake --build . --target all


6. Run unit tests
  
        $ ctest --verbose
        
## Conan package

[Conan](https://docs.conan.io/en/latest) is an opena source package manager for C/C++. [Bintray shadow](https://bintray.com/lukaszlaszko/shadow)
repository provides latest redistributable package with project artefacts. In order to use it in your cmake based project:

1. Add following block to root `CMakeLists.txt` of your project:

        if(EXISTS ${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
            include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
            conan_basic_setup()
        else()
            message(WARNING "The file conanbuildinfo.cmake doesn't exist, you have to run conan install first")
        endif()
    
    
2. Add `conanfile.txt` side by side with your top level `CMakeLists.txt`:
    
        [requires]
        rendezvous/1.5@shadow/stable
        
        [generators]
        cmake   
    
4. Add `shadow` to your list of conan remotes:
    
        $ conan remote add shadow https://api.bintray.com/conan/lukaszlaszko/shadow     
    
3. Install conan dependencies into your cmake build directory:

        $ conan install . -if <build dir>   
    
    if for whatever reason installation of binary package fails, add `--build=missing` flag to the above. This will perform install the source package and compile all necessary modules.      
    
4. To link against libraries provided by the package, either add:

        target_link_libraries([..] ${CONAN_LIBS}) 
    
    for your target. Or specifically:
    
        target_link_libraries([..] ${CONAN_LIBS_RENDEZVOUS})    
    
5. Reload cmake configuration.

## Troubleshooting

If during compilation with GCC > 5 error like this pops up:
```
undefined reference to `testing::internal::GetBoolAssertionFailureMessage[abi:cxx11](testing::AssertionResult const&, char const*, char const*, char const*)
```

make sure conan profile used for gtest installation is configured to use `libstdc++11`. This can be done either by setting **compiler.libcxx=libstdc++11** in `~/.conan/profiles/default` or by specifying it from command line.

Reference - https://docs.conan.io/en/latest/howtos/manage_gcc_abi.html  
