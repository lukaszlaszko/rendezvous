#pragma once

#include <boost/memory.hpp>

#include <framework/transports.hpp>


class handler
{
public:
    void on_connected(
            framework::ipv4_tcp_server::single_sender::client_transport& client);
    void on_disconnected(
            framework::ipv4_tcp_server::single_sender::client_transport& client);
    void on_received(
            framework::ipv4_tcp_server::single_sender::client_transport& client, 
            boost::memory::buffer_ref& buffer);
};

