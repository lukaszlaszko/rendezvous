#include "handler.hpp"

#include <iostream>

using namespace std;
using namespace boost::memory;
using namespace framework;
using namespace framework::ipv4_tcp_server::single_sender;


void handler::on_connected(client_transport& client)
{
    cout << "connected: " << client << endl;
    client.send(string("Welcome.Start typing to see the echo\n"));
}

void handler::on_disconnected(client_transport& client)
{
    cout << "disconnected: " << client << endl;
}

void handler::on_received(client_transport& client, buffer_ref& buffer)
{
    client.send(buffer);
}

