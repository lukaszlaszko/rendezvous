#include "handler.hpp"

#include <iostream>

using namespace std;
using namespace framework;
using namespace boost::memory;

void handler::on_connected(transport_type& client)
{
    cout << "connected: " << client << endl;
    transport_ = client;
}

void handler::on_disconnected(transport_type& client)
{
    cout << "disconnected: " << client << endl;
    transport_ = boost::none;
}

void handler::on_received(transport_type& client, buffer_ref& buffer)
{
    cout << string(buffer.as_pointer<char*>(), buffer.length());
}

void handler::on_send(std::string& line)
{
    if (transport_)
        transport_->send(move(line));
}

