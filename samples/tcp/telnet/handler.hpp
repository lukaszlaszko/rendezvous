#pragma once

#include <framework/transports.hpp>

#include <boost/memory.hpp>
#include <boost/optional.hpp>


class handler
{
public:
    using transport_type = framework::ipv4_tcp_client::single_sender::client_transport;
    
    void on_connected(transport_type& client);
    void on_disconnected(transport_type& client);
    void on_received(transport_type& client, boost::memory::buffer_ref& buffer);
    
    void on_send(std::string& line);
    
private:
    boost::optional<transport_type&> transport_ {boost::none};
};

