#pragma once

#include <boost/memory.hpp>

#include <framework/transports.hpp>


class handler
{
public:
    void on_received(
            framework::ipv4_udp::single_sender::server_transport& source, 
            boost::memory::buffer_ref& buffer);
};

