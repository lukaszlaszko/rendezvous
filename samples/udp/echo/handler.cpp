#include "handler.hpp"

#include <iostream>

using namespace std;
using namespace boost::memory;
using namespace framework;
using namespace framework::ipv4_udp::single_sender;

void handler::on_received(server_transport& source, buffer_ref& buffer)
{
    source.reply(buffer);
}

