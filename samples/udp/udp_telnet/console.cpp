#include "console.hpp"

#include <stdio.h>
#include <string>
#include <unistd.h>

using namespace std;
using namespace framework;


console::console(
        dispatcher& dispatcher,
        read_callback_type read_callback,
        error_callback_type error_callback)
    :
        dispatchable(error_callback),
        dispatcher_(dispatcher),
        read_callback_(read_callback)
{
    dispatchable::set_fd(STDIN_FILENO);
    dispatcher_.add(*this, true, false, false, false);
}

console::~console()
{
    dispatcher_.remove(*this);
}

void console::handle_event(
        bool should_receive,
        bool should_send,
        bool should_disconnect)
{
    if (should_receive)
    {
        char buffer[read_size];
        auto read_result = read(fd(), &buffer, sizeof(buffer));
        if (read_result > 0)
        {
            string line(buffer, read_result);
            if (read_callback_)
                read_callback_(line);
        }
    }
}
