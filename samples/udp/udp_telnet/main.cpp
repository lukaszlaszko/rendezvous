#include "console.hpp"

#include <framework/dispatcher.hpp>
#include <framework/loop.hpp>
#include <framework/transports.hpp>

#include <boost/program_options.hpp>

#include <cstdlib>
#include <exception>
#include <iostream>
#include <system_error>

using namespace std;
using namespace std::placeholders;
using namespace boost;
using namespace boost::memory;

static constexpr auto help_parameter = "help,h";
static constexpr auto address_parameter = "address,a";
static constexpr auto port_parameter = "port,p";

static constexpr auto result_success = 0;
static constexpr auto error_in_command_line = 1;
static constexpr auto error_unandled_exception = 2;

int main(int argc, char** argv)
{
    try
    {
        bool help(false);
        string address;
        uint16_t port;

        program_options::options_description description("Options");
        description.add_options()
            (help_parameter,
                program_options::bool_switch(&help),
                "Print help messages")
            (address_parameter,
                program_options::value<string>(&address)->required(),
                "Remote address to connect.")
            (port_parameter,
                program_options::value<uint16_t>(&port)->required(),
                "Port to connect at.");

        program_options::variables_map variables;
        try
        {
            program_options::store(
                    program_options::parse_command_line(argc, argv, description),
                    variables);

            if (help)
            {
                cout << "Telnet:" << endl;
                cout << description << endl;

                return result_success;
            }

            program_options::notify(variables);
        }
        catch(boost::program_options::error& e)
        {
            cerr << "ERROR: " << e.what() << endl << endl;
            cerr << description << endl;

            return error_in_command_line;
        }

        framework::dispatcher dispatcher;
        framework::loop main_loop(dispatcher);

        framework::udp_options options;
        framework::ipv4_udp::single_sender::client_transport transport(
                dispatcher,
                options,
                { address, port },
                [](auto& source, buffer_ref& buffer)
                {
                    cout << string(buffer.as_pointer<char*>(), buffer.length());
                },
                [](auto& error)
                {
                    cerr << error.what() << endl;
                });

        console console_instance(
                dispatcher,
                [&transport](auto& line)
                {
                    transport.send(line);
                });

        main_loop.interrupt_on(SIGTERM);
        main_loop.interrupt_on(SIGQUIT);
        main_loop.interrupt_on(SIGINT);
        main_loop.run_forever();

        return result_success;
    }
    catch(std::exception& e)
    {
        cerr << "Unhandled Exception reached the top of main: ";
        cerr << e.what() << ", application will now exit" << endl;

        return error_unandled_exception;
    }
}

