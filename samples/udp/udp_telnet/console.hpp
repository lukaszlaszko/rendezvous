#pragma once

#include <framework/dispatchable.hpp>
#include <framework/dispatcher.hpp>

#include <functional>
#include <string>


class console : public framework::dispatchable
{
public:
    using read_callback_type = std::function<void(std::string&)>;
    
    explicit console(
            framework::dispatcher& dispatcher,
            read_callback_type read_callback,
            error_callback_type error_callback = {});
    virtual ~console();
    
private:
    static constexpr size_t read_size = 1024;
    
    virtual void handle_event(
            bool should_receive, 
            bool should_send, 
            bool should_disconnect) override;
    
    framework::dispatcher& dispatcher_;
    read_callback_type read_callback_;
};

