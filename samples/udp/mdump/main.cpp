#include "handler.hpp"

#include <framework/dispatcher.hpp>
#include <framework/loop.hpp>
#include <framework/transports.hpp>

#include <boost/program_options.hpp>

#include <cstdlib>
#include <exception>
#include <iostream>

using namespace std;
using namespace std::placeholders;
using namespace boost;

static constexpr auto help_parameter = "help,h";
static constexpr auto address_parameter = "address,a";
static constexpr auto port_parameter = "port,p";
static constexpr auto interface_paramater = "interface,i";

static constexpr auto default_interface = "0.0.0.0";

static constexpr auto result_success = 0;
static constexpr auto error_in_command_line = 1;
static constexpr auto error_unandled_exception = 2;


int main(int argc, char** argv)
{
    try
    {
        bool help(false);
        string address;
        uint16_t port;
        string interface;

        program_options::options_description description("Options");
        description.add_options()
            (help_parameter,
                program_options::bool_switch(&help),
                "Print help messages")
            (address_parameter,
                program_options::value<string>(&address)->required(),
                "Multicast group address")
            (port_parameter,
                program_options::value<uint16_t>(&port)->required(),
                "Multicast port")
            (interface_paramater,
                program_options::value<string>(&interface)->default_value(default_interface),
                "Interface to listen on");

        program_options::variables_map variables;
        try
        {
            program_options::store(
                    program_options::parse_command_line(argc, argv, description),
                    variables);

            if (help)
            {
                cout << "mdump:" << endl;
                cout << description << endl;

                return result_success;
            }

            program_options::notify(variables);
        }
        catch(boost::program_options::error& e)
        {
            cerr << "ERROR: " << e.what() << endl << endl;
            cerr << description << endl;

            return error_in_command_line;
        }

        framework::dispatcher dispatcher;
        framework::loop main_loop(dispatcher);

        handler handler_instance;

        framework::udp_options options;
        framework::ipv4_udp::listener_transport transport(
                dispatcher,
                options,
                { address, port },
                interface,
                bind(&handler::on_received, &handler_instance, _1, _2));

        cout << "started " << transport << endl;

        main_loop.interrupt_on(SIGTERM);
        main_loop.interrupt_on(SIGQUIT);
        main_loop.interrupt_on(SIGINT);
        main_loop.run_forever();

        cout << "stopping..." << endl;

        return result_success;
    }
    catch(std::exception& e)
    {
        cerr << "Unhandled Exception reached the top of main: ";
        cerr << e.what() << ", application will now exit" << endl;

        return error_unandled_exception;
    }
}

