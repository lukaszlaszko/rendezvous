#pragma once

#include <boost/memory.hpp>

#include <framework/transports.hpp>


struct handler
{   
    void on_received(framework::ipv4_udp::listener_transport& source, boost::memory::buffer_ref& buffer);
};
