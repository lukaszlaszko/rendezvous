#include "handler.hpp"

#include <iostream>

using namespace std;
using namespace framework;
using namespace boost::memory;


void handler::on_received(ipv4_udp::listener_transport& source, buffer_ref& buffer)
{
    cout << "received: " <<  buffer.length() << " bytes from: " << source.info().from()  << endl;
}

