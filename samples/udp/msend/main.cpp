#include <framework/dispatcher.hpp>
#include <framework/loop.hpp>
#include <framework/timers.hpp>
#include <framework/transports.hpp>

#include <boost/program_options.hpp>

#include <chrono>
#include <cstdlib>
#include <ctime>
#include <exception>
#include <iomanip>
#include <iostream>

using namespace std;
using namespace std::chrono;
using namespace std::placeholders;
using namespace boost;

static constexpr auto help_parameter = "help,h";
static constexpr auto address_parameter = "address,a";
static constexpr auto port_parameter = "port,p";
static constexpr auto interface_paramater = "interface,i";
static constexpr auto interval_parameter = "interval,t";

static constexpr auto default_interface = "0.0.0.0";
static constexpr auto default_interval = 1;

static constexpr auto result_success = 0;
static constexpr auto error_in_command_line = 1;
static constexpr auto error_unandled_exception = 2;


int main(int argc, char** argv)
{
    try
    {
        bool help(false);
        string address;
        uint16_t port;
        string interface;
        uint64_t interval_sec;

        program_options::options_description description("Options");
        description.add_options()
            (help_parameter,
                program_options::bool_switch(&help),
                "Print help messages")
            (address_parameter,
                program_options::value<string>(&address)->required(),
                "Multicast group address")
            (port_parameter,
                program_options::value<uint16_t>(&port)->required(),
                "Multicast port")
            (interface_paramater,
                program_options::value<string>(&interface)->default_value(default_interface),
                "Interface to publish on")
            (interval_parameter,
                program_options::value<uint64_t>(&interval_sec)->default_value(default_interval),
                "Send interval in seconds");

        program_options::variables_map variables;
        try
        {
            program_options::store(
                    program_options::parse_command_line(argc, argv, description),
                    variables);

            if (help)
            {
                cout << "msend:" << endl;
                cout << description << endl;

                return result_success;
            }

            program_options::notify(variables);
        }
        catch(boost::program_options::error& e)
        {
            cerr << "ERROR: " << e.what() << endl << endl;
            cerr << description << endl;

            return error_in_command_line;
        }

        framework::dispatcher dispatcher;
        framework::loop main_loop(dispatcher);

        framework::udp_options options;
        framework::ipv4_udp::single_sender::publisher_transport transport(
                dispatcher,
                options,
                { address, port },
                interface,
                [](auto& error)
                {
                    cerr << error << endl;
                });

        seconds interval(interval_sec);
        framework::auto_reload_timer timer(
                dispatcher,
                interval,
                [&transport](auto& source, auto expirations)
                {
                    auto now = system_clock::now();
                    auto now_c = system_clock::to_time_t(now);

                    stringstream ss;
                    ss << put_time(localtime(&now_c), "%F %T");

                    transport.send(ss.str());
                },
                [](auto& error)
                {
                    cerr << error << endl;
                });
        timer.arm();

        cout << "started " << transport << endl;

        main_loop.interrupt_on(SIGTERM);
        main_loop.interrupt_on(SIGQUIT);
        main_loop.interrupt_on(SIGINT);
        main_loop.run_forever();

        cout << "stopping..." << endl;

        return result_success;
    }
    catch(std::exception& e)
    {
        cerr << "Unhandled Exception reached the top of main: ";
        cerr << e.what() << ", application will now exit" << endl;

        return error_unandled_exception;
    }
}

