from conans import ConanFile, CMake, tools


class RendezvousConan(ConanFile):
    name = "rendezvous"
    version = "1.5"
    license = "Boost Software License - Version 1.0"
    author = "Lukasz Laszko lukaszlaszko@gmail.com"
    url = "https://bitbucket.org/lukaszlaszko/rendezvous"
    description = "C++ infrastructure library for writing applications around event loop(s)."
    topics = ("event loop", "epoll", "networking", "c++", "cpp", "", "cxx")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"
    exports_sources = ["CMakeLists.txt", "src/*", "external/*", "tests/*", "docs/*", "samples/*"]
    build_requires = "gtest/1.8.1@bincrafters/stable"
    requires = "allocators/1.2@shadow/stable", "circular_buffers/1.3@shadow/stable", "boost/1.69.0@conan/stable"

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy("*.hpp", dst="include", src="src/include")
        self.copy("*.ipp", dst="include", src="src/include")

    def package_info(self):
        self.cpp_info.libs = ["rendezvous"]
